package  
{
	import com.greensock.TweenNano;
	import com.shirne.component.FixedMenu;
	import com.shirne.component.FullScreen;
	import com.shirne.component.Image;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.filters.DropShadowFilter;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.printing.PrintJob;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.text.TextFormat;
	import flash.ui.Mouse;
	import flash.ui.MouseCursor;
	import flash.utils.CompressionAlgorithm;
	import nid.FlipBook;
	
	/**
	 * ...
	 * @author Shirne
	 */
	public class Main extends Sprite 
	{
		
		private var flipBook:FlipBook;
		private var flipBox:Sprite;
		private var flipTn:TweenNano;
		
		private var flipReady:Boolean = false;
		
		public static var toolHeight:Number = 30;
		
		private var toolBar:Sprite;
		
		private var xml:XML;
		
		private var bmpds:Object = { };
		
		//工具栏背景
		[Embed(source="/../lib/floater.png")]
		public static var toolbgBmp:Class;
		public static var toolbg:BitmapData;
		
		toolbg = new toolbgBmp().bitmapData;
		
		//全屏按钮
		private var fullScreen:FullScreen;
		
		//上一页,下一页
		private var prevPage:FButton;
		private var nextPage:FButton;
		
		//跳转页
		private var pageInputBox:Sprite;
		private var pageInput:TextField;
		private var gotoPage:FButton;
		private var pageState:TextField;	//页码状态
		
		//缩放按钮
		private var zoomBig:FButton;
		private var zoomSmall:FButton;
		private var zoomFix:FButton;
		private var print:FButton;
		
		private var zoom:Number = 1;
		
		//播放暂停按钮
		private var playButton:FButton;
		
		private var oWidth:Number;
		private var oHeight:Number;
		
		
		private static var tips:Sprite;
		private static var tipShow:Boolean = false;
		
		private static var main:Main;
		
		//位图资源
		[Embed(source = "/../lib/arrow.png")]
		public static var arrowData:Class;
		
		[Embed(source = "/../lib/Go.png")]
		public static var GoData:Class;
		
		[Embed(source = "/../lib/input.png")]
		public static var inputData:Class;
		
		[Embed(source = "/../lib/zoomBig.png")]
		public static var zoomBigData:Class;
		
		[Embed(source = "/../lib/zoomSmall.png")]
		public static var zoomSmallData:Class;
		
		[Embed(source = "/../lib/zoomFix.png")]
		public static var zoomFixData:Class;
		
		[Embed(source = "/../lib/print.png")]
		public static var printData:Class;
		
		public function Main() 
		{
			if (main) throw new Error("");
			main = this;
			this.addEventListener(Event.ADDED_TO_STAGE, initialize);
		}
		
		public function initialize(e:Event):void {
			this.removeEventListener(Event.ADDED_TO_STAGE, initialize);
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			stage.addEventListener(Event.RESIZE, resizer);
			
			new FixedMenu(this, "临风小筑", "http://www.shirne.com");
			
			tips = new Sprite();
			addChild(tips);
			
			flipBook = new FlipBook();
			flipBook.filters = [new DropShadowFilter(2,45,0,.6,10,10)];
			
			toolBar = new Sprite();
			
			fullScreen = new FullScreen(0xffffff, "");
			fullScreen.y = 5;
			toolBar.addChild(fullScreen);
			
			var arrow:BitmapData = new arrowData().bitmapData;
			var pbmp:Bitmap = new Bitmap(arrow);
			pbmp.rotationY = -180;
			pbmp.x = pbmp.width;
			prevPage = new FButton("上一页", pbmp, goPrevPage);
			prevPage.y = 5;
			toolBar.addChild(prevPage);
			
			gotoPage = new FButton('跳转到', new GoData(), jumpPage);
			gotoPage.y = 5;
			toolBar.addChild(gotoPage);
			
			
			pageInputBox = new Sprite();
			pageInput = new TextField();
			pageInput.autoSize = TextFieldAutoSize.LEFT;
			pageInput.type = TextFieldType.INPUT;
			pageInput.defaultTextFormat = new TextFormat("微软雅黑",12,0x333333);
			
			var id:BitmapData = new inputData().bitmapData;
			pageInputBox.graphics.beginBitmapFill(id);
			pageInputBox.graphics.drawRect(0, 0, id.width, id.height);
			pageInputBox.graphics.endFill();
			
			pageInput.scrollRect = new Rectangle(0,0,id.width,pageInput.height);
			pageInputBox.addChild(pageInput);
			pageInputBox.y = 6;
			toolBar.addChild(pageInputBox);
			
			pageState = new TextField();
			pageState.autoSize = TextFieldAutoSize.LEFT;
			pageState.defaultTextFormat = new TextFormat("微软雅黑",12,0xffffff);
			pageState.text = "0/0";
			pageState.scrollRect = new Rectangle(0, 0, 40, pageState.height);
			pageState.y = (toolHeight - pageState.height) * .5;
			toolBar.addChild(pageState);
			
			
			nextPage = new FButton("下一页", new Bitmap(arrow), goNextPage);
			nextPage.y = 5;
			toolBar.addChild(nextPage);
			
			zoomBig = new FButton("放大", new zoomBigData(), doZoomBig);
			zoomBig.y = 5;
			zoomBig.x = 50;
			toolBar.addChild(zoomBig);
			
			zoomSmall = new FButton("缩小", new zoomSmallData(), doZoomSmall);
			zoomSmall.y = 5;
			zoomSmall.x = zoomBig.x + zoomBig.width + 10;
			toolBar.addChild(zoomSmall);
			
			zoomFix = new FButton("适合屏幕", new zoomFixData(), doZoomFix);
			zoomFix.y = 5;
			zoomFix.x = zoomSmall.x + zoomSmall.width + 10;
			toolBar.addChild(zoomFix);
			
			print = new FButton("打印本页", new printData(), doPrint);
			print.y = 5;
			print.x = zoomFix.x + zoomFix.width + 10;
			toolBar.addChild(print);
			
			addChild(toolBar);
			
			resizer(new Event(Event.RESIZE));
			
			load(stage.loaderInfo.parameters.data == undefined?'data.xml':stage.loaderInfo.parameters.data);
		}
		
		public function load(url:String):void 
		{
			var loader:URLLoader = new URLLoader();
			loader.addEventListener(Event.COMPLETE, onComplete);
			loader.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
			loader.load(new URLRequest(url));
		}
		
		private function onIOError(e:IOErrorEvent):void 
		{
			
		}
		private function onComplete(e:Event=null):void 
		{
			xml = new XML(e.currentTarget.data);
			if(String(xml.site)){
				new FixedMenu(this, String(xml.site.name), String(xml.site.url));
			}
			flipBook.xml = xml;
			flipBook.addEventListener(Event.INIT, flipInit);
			flipBook.addEventListener(Event.CHANGE, pageChange);
			
			flipBox = new Sprite();
			flipBox.addChild(flipBook);
			flipBox.addEventListener(MouseEvent.MOUSE_OVER, flipHand);
			flipBox.addEventListener(MouseEvent.MOUSE_OUT, flipHandRestore);
			addChild(flipBox);
			setChildIndex(flipBox, 0);
			
			this.addEventListener(Event.ENTER_FRAME, enterFrame);
			this.addEventListener(MouseEvent.MOUSE_MOVE, mouseMove);
		}
		
		private function enterFrame(e:Event):void {
			if (tips) {
				if (tipShow) {
					if(tips.alpha<1)tips.alpha += .1;
				}else {
					if(tips.alpha>0)tips.alpha -= .1;
				}
			}
		}
		private function mouseMove(e:MouseEvent):void {
			tips.x = this.mouseX+10;
			tips.y = this.mouseY-10;
		}
		
		public static function updateTip(text:String="") :void{
			tips.parent.setChildIndex(tips, tips.parent.numChildren - 1);
			var t:TextField;
			if (tips.numChildren < 1) {
				t = new TextField();
				t.defaultTextFormat = new TextFormat("微软雅黑", 12, 0);
				t.autoSize = TextFieldAutoSize.LEFT;
				t.x = 5;
				tips.addChild(t);
			}else {
				t = tips.getChildAt(0) as TextField;
			}
			if(text!="")t.text = text;
			
			tips.graphics.clear();
			tips.graphics.lineStyle(1, 0x000000, .5);
			tips.graphics.beginFill(0xffffff, .5);
			tips.graphics.drawRoundRect(0, 0, t.width + 10, t.textHeight+5, 5);
			tips.graphics.endFill();
			
			tipShow = true;
			tips.visible = true;
		}
		public static function hideTip():void {
			tipShow = false;
			//tips.visible = false;
		}
		
		/**
		 * 翻页
		 * @param	e
		 */
		private function goPrevPage(e:MouseEvent):void {
			flipBook.prevPage();
		}
		
		private function goNextPage(e:MouseEvent):void {
			flipBook.nextPage();
		}
		
		/**
		 * 跳转
		 * @param	e
		 */
		private function jumpPage(e:MouseEvent):void {
			var gpage:int = int(pageInput.text);
			
			flipBook.gotoPage(gpage,true);
		}
		
		/**
		 * 缩放
		 * @param	e
		 */
		private function doZoomBig(e:MouseEvent):void {
			if ( zoom < 3) {
				zoom += .2;
			}
			resizer();
			if (zoom <= 1) {
				noMove();
			}else {
				canMove();
			}
		}
		
		private function doZoomSmall(e:MouseEvent):void {
			if ( zoom > .4) {
				zoom -= .2;
			}
			resizer();
			if (zoom <= 1) {
				noMove();
			}else {
				canMove();
			}
		}
		
		private function doZoomFix(e:MouseEvent):void {
			zoom = 1;
			resizer();
			noMove();
		}
		
		private function doPrint(e:MouseEvent):void {
			var pj:PrintJob = new PrintJob();
			if(pj.start()){
			
				pj.addPage(flipBook.currentPage[0]);
				
				if (flipBook.currentPage.length > 1) pj.addPage(flipBook.currentPage[1]);
				
				pj.send();
			}
		}
		
		/**
		 * 取消手动移动
		 */
		private function noMove():void {
			if (flipBox.mouseChildren) {
				return;
			}
			flipBox.mouseChildren = true;
			flipBox.removeEventListener(MouseEvent.MOUSE_DOWN, downHandle);
			
			flipBox.x = (stage.stageWidth - FlipBook.PAGE_WIDTH*2) * .5;
			flipBox.y = (stage.stageHeight - toolHeight - FlipBook.PAGE_HEIGHT) * .5;
		}
		/**
		 * 开启手动移动
		 */
		private function canMove():void {
			if (flipBox.mouseChildren == false ) {
				return;
			}
			flipBox.mouseChildren = false;
			flipBox.addEventListener(MouseEvent.MOUSE_DOWN, downHandle);
		}
		
		/**
		 * 移动的鼠标手势
		 * @param	e
		 */
		private function flipHand(e:MouseEvent):void {
			if (zoom > 1) {
				Mouse.cursor = MouseCursor.HAND;
			}else {
				Mouse.cursor = MouseCursor.AUTO;
			}
		}
		
		private function flipHandRestore(e:MouseEvent):void {
			Mouse.cursor = MouseCursor.AUTO;
		}
		
		/**
		 * 移动操作
		 * @param	e
		 */
		private function downHandle(e:MouseEvent):void {
			var sx:Number = e.stageX, sy:Number = e.stageY;
			var bx:Number = flipBox.x, by:Number = flipBox.y;
			
			var moveHandle:Function = function(e:MouseEvent):void {
				flipBox.x = bx + e.stageX - sx;
				flipBox.y = by + e.stageY - sy;
			};
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE, moveHandle);
			stage.addEventListener(MouseEvent.MOUSE_UP, function(e:MouseEvent):void {
				stage.removeEventListener(MouseEvent.MOUSE_MOVE, moveHandle);
				stage.removeEventListener(MouseEvent.MOUSE_UP, arguments.callee);
			});
		}
		
		/**
		 * 页码变动
		 * @param	e
		 */
		private function pageChange(e:Event=null):void {
			pageState.text = flipBook.page + '/' + flipBook.totalPages;
			//trace(flipTn);
			if (flipTn) {
				flipTn.kill();
				flipTn = null;
			}
			if (flipBook.page == 0 && flipBook.x >= 0) {
				
				flipTn = TweenNano.to(flipBook, .5, { x: -FlipBook.PAGE_WIDTH*.5, onComplete:function():void {
					flipTn.kill();
				}});
			}
			else if (flipBook.page == flipBook.totalPages && flipBook.x <= 0) {
				flipTn = TweenNano.to(flipBook, .5, { x: FlipBook.PAGE_WIDTH*.5, onComplete:function():void {
					flipTn.kill();
				}});
			}
			else if (flipBook.x != 0) {
				flipTn = TweenNano.to(flipBook, .5, { x: 0, onComplete:function():void {
					flipTn.kill();
				}});
			}
		}
		
		public function flipInit(e:Event):void {
			flipReady = true;
			
			pageChange();
			
			resizer(e);
		}
		
		public function resizer(e:Event = null):void {
			if(xml && xml.settings.background.length()>0){
				graphics.clear();
				var bg:XML = xml.settings.background[0];
				if(bg.color.length()>0){
					//画背景色
					var mtx:Matrix = new Matrix();
					var color:XML = bg.color[0];
					var startColor:uint = parseInt(color.start), endColor:uint = parseInt(color.end);
					if (String(color.@type).toUpperCase() == 'RADIAL') {
						mtx.createGradientBox(stage.stageWidth, stage.stageHeight);
						graphics.beginGradientFill(GradientType.RADIAL, [startColor, endColor], [1, 1], [1, 255], mtx);
					}else {
						if (String(color.@direction).toUpperCase() == "VERTICAL") {
							mtx.createGradientBox(stage.stageWidth, stage.stageHeight, Math.PI * .5);
						}else if (String(color.@direction).toUpperCase() == "CUSTOM") {
							mtx.createGradientBox(stage.stageWidth, stage.stageHeight, Math.PI * parseInt(color.@rotation)/360 );
						}else {
							mtx.createGradientBox(stage.stageWidth, stage.stageHeight);
						}
						graphics.beginGradientFill(GradientType.LINEAR, [startColor, endColor], [1, 1], [1, 255], mtx);
					}
					graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
					graphics.endFill();
				}
				
				var images:XMLList = bg.img;
				if (images.length() > 0 ) {
					for (var i:int = 0; i < images.length(); i++) {
						if(String(images[i])){
							if (bmpds[String(images[i])]) {
								paintImage(graphics, bmpds[String(images[i])], images[i]);
							}else {
								loadAndPaint(graphics,images[i]);
							}
						}
					}
				}
			}
			
			if(e){
				toolBar.graphics.clear();
				toolBar.graphics.beginBitmapFill(toolbg);
				toolBar.graphics.drawRect(0, 0, stage.stageWidth, toolHeight);
				toolBar.graphics.endFill();
				toolBar.x = 0;
				toolBar.y = stage.stageHeight - toolHeight;
				
				fullScreen.x = stage.stageWidth - 10 - fullScreen.width;
				nextPage.x = fullScreen.x - nextPage.width - 20;
				gotoPage.x = nextPage.x - gotoPage.width - 10;
				pageInputBox.x = gotoPage.x - pageInputBox.width - 10;
				pageState.x = pageInputBox.x - pageState.width - 10;
				prevPage.x = pageState.x - prevPage.width - 10;
			
				if (!flipReady) {
					return;
				}
				var s:Number = Math.min((stage.stageWidth - 70)*.5 / FlipBook.PAGE_WIDTH, (stage.stageHeight - toolHeight - 20) / FlipBook.PAGE_HEIGHT);
				oWidth = FlipBook.PAGE_WIDTH * s;
				oHeight = FlipBook.PAGE_HEIGHT * s;
				
				zoom = 1;
				noMove();
			}
			if (!flipReady) {
				return;
			}
			FlipBook.PAGE_WIDTH = oWidth * zoom;
			FlipBook.PAGE_HEIGHT = oHeight * zoom;
			flipBook.reset();
			flipBook.resetPages();
			
			//flipBook.render();
				
			flipBox.x = (stage.stageWidth - FlipBook.PAGE_WIDTH * 2) * .5;
			flipBox.y = (stage.stageHeight - toolHeight - FlipBook.PAGE_HEIGHT) * .5;
			
		}
		
		private function loadAndPaint(g:Graphics,sets:XML):void {
			Image.loadImage(String(sets), function(bmp:BitmapData):void {
				bmpds[String(sets)] = bmp;
				paintImage(graphics, bmp, sets);
			});
		}
		
		private function paintImage(g:Graphics,bmp:BitmapData,sets:XML):void {
			var sw:int = stage.stageWidth, sh:int = stage.stageHeight,ss:Number=sw/sh;
			var bw:int = bmp.width, bh:int = bmp.height, bs:Number = bw/bh, ms:Number=1;
			var repeat:Boolean = String(sets.@repeat).toLowerCase() == "true";
			switch (String(sets.@resize)) {
				case "inner":
					if (bs > ss) {
						ms = sw / bw;
					}else {
						ms = sh / bh;
					}
					break;
				case "outer":
					if (bs > ss) {
						ms = sh / bh;
					}else {
						ms = sw / bw;
					}
					break;
				default:
					
			}
			var mtx:Matrix = new Matrix();
			var tw:Number = 0, th:Number = 0;
			switch(String(sets.@align)) {
				case StageAlign.BOTTOM,"BOTTOM":
					tw = (sw - bw*ms) * .5;
					th = sh - bh*ms;
					break;
				case StageAlign.BOTTOM_LEFT,"BOTTOM_LEFT":
					th = sh - bh*ms;
					break;
				case StageAlign.BOTTOM_RIGHT,"BOTTOM_RIGHT":
					tw = sw - bw*ms;
					th =  sh - bh*ms;
					break;
				case StageAlign.LEFT,"LEFT":
					th = (sh - bh*ms) * .5;
					break;
				case StageAlign.RIGHT,"RIGHT":
					tw = sw - bw*ms;
					th = (sh - bh*ms) * .5;
					break;
				case StageAlign.TOP,"TOP":
					tw = (sw - bw*ms) * .5;
					break;
				case StageAlign.TOP_LEFT,"TOP_LEFT":
					//
					break;
				case StageAlign.TOP_RIGHT,"TOP_RIGHT":
					tw = sw - bw*ms;
					break;
				default:
					tw = (sw - bw*ms) * .5;
					th =  (sh - bh*ms) * .5;
					break;
			}
			if (ms != 1) mtx.scale(ms, ms);
			mtx.translate(tw, th);
			graphics.beginBitmapFill(bmp, mtx, repeat, true);
			if(repeat){
				graphics.drawRect(0, 0, sw, sh);
			}else {
				graphics.drawRect(tw, th, bw*ms, bh*ms);
			}
			graphics.endFill();
		}
	}

}