package  
{
	import com.greensock.TweenNano;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filters.GlowFilter;
	
	/**
	 * 
	 * @author Shirne
	 */
	public class FButton extends Sprite 
	{
		//private var tn:TweenNano;
		//private var filter:GlowFilter;
		
		private var cHandle:Function;
		private var text:String="按钮说明";
		
		public override function get height():Number {
			return 20;
		}
		
		public function FButton(txt:String, style:DisplayObject, handle:Function = null ) 
		{
			style.y = (20 - style.height) * .5;
			this.addChild(style);
			if (txt) this.text = txt;
			
			this.buttonMode = true;
			
			this.addEventListener(MouseEvent.CLICK, clickHandle);
			this.addEventListener(MouseEvent.MOUSE_DOWN, noAction);
			this.addEventListener(MouseEvent.MOUSE_UP, noAction);
			if (handle != null) {
				cHandle = handle;
			}
			
			//filter = new GlowFilter(0xffffff, .6, 0, 0, 0);
			
			this.addEventListener(MouseEvent.MOUSE_OVER, hover);
			this.addEventListener(MouseEvent.MOUSE_OUT, normal);
		}
		
		public function clickHandle(e:MouseEvent):void {
			e.stopPropagation();
			e.preventDefault();
			cHandle(e);
		}
		
		public function noAction(e:MouseEvent):void {
			e.stopPropagation();
			//e.preventDefault();
		}
		
		public function normal(e:Event):void {
			/*if (tn) {
				tn.kill();
			}
			tn = TweenNano.to(filter,1,{blurX:0,blurY:0,strength:0,onUpdate:applyFilter});*/
			Main.hideTip();
		}
		
		public function hover(e:Event):void {
			/*if (tn) {
				tn.kill();
			}
			tn = TweenNano.to(filter,1,{blurX:8,blurY:8,strength:4,onUpdate:applyFilter});*/
			Main.updateTip(this.text);
		}
		
		private function applyFilter(e:Event=null):void{
			//this.filters=[filter];
		}
	}

}