package nid 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Shape;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Nidin P Vinayakan
	 */
	public class Preloader extends Sprite 
	{
		public var bg:Shape;
		public var bar:Shape;
		public var logo:Bitmap;
		
		private var _width:Number = 200;
		
		[Embed(source = "/../lib/logo.png")]
		public static var logoClass:Class;
		public static var logoData:BitmapData;
		
		logoData = new logoClass().bitmapData;
		
		public override function get width():Number {
				return _width;
		}
		
		public function Preloader() 
		{
			configUI();
		}
		
		private function configUI():void 
		{
			logo = new Bitmap(logoData);
			logo.smoothing = true;
			logo.y = -logoData.height*.5-8;
			logo.x = (_width - logoData.width) * .5;
			addChild(logo);
			
			bg = new Shape();
			bg.graphics.beginFill(0xCCCCCC);
			bg.graphics.drawRect(0, logoData.height*.5, _width, 2);
			bg.graphics.endFill();
			addChild(bg);
			
			bar = new Shape();
			bar.graphics.beginFill(0x333333);
			bar.graphics.drawRect(0, 0, _width, 2);
			bar.graphics.endFill();
			bar.y = logoData.height*.5;
			addChild(bar);
			bar.width = 1;
			
		}
		public function progress(percent:int):void
		{
			bar.width = percent * _width * .01;
		}
	}

}