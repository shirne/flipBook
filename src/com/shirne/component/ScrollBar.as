package com.shirne.component 
{
	import com.greensock.easing.*;
	import com.greensock.TweenLite;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Rectangle;
	import flash.utils.Timer;
	import com.txmdtea.TSDQSClient.*;
	/**
	 * 滚动条
	 * @author Shirne
	 */
	public class ScrollBar extends Sprite 
	{
		private var target:DisplayObject;
		
		private var targetWidth:Number=0;
		private var targetHeight:Number = 0;
		
		private var scrollPercent:Number=0;
		
		private var useArrow:Boolean;
		
		private var arrowUp:Sprite;
		private var arrowDown:Sprite;
		private var scroll:Sprite;
		
		private var color:uint = 0x666666;
		
		private var timer:Timer;
		
		private var _size:Number = 4;
		
		private var startY:Number
		
		private var endY:Number;
		
		private var scrollTween:TweenLite;
		private var targetTween:TweenLite;
		private var targetRect:Rectangle;
		
		/**
		 * 创建滚动条,并且创建对象上的拖动滚动
		 * @param	target	目标对象
		 * @param	rect	滚动范围
		 * @param	arrow	是否显示上下箭头
		 * @param	refresh	刷新间隔
		 */
		public function ScrollBar(target:DisplayObject, rect:Rectangle, arrow:Boolean=true, refresh:Number=500):void 
		{
			this.target = target;
			this.target.scrollRect = rect;
			this.useArrow = arrow;
			
			this.target.parent.addChild(this);
			targetRect = this.target.scrollRect;
			
			target.addEventListener(MouseEvent.MOUSE_DOWN, dragScroll);
			
			timer = new Timer(refresh);
			timer.addEventListener(TimerEvent.TIMER, update);
			timer.start();
			target.addEventListener(Event.RESIZE, setStyle);
			setStyle();
		}
		
		//初始样式
		public function setStyle(e:Event=null):void {
			var rect:Rectangle = target.scrollRect;
			if (this.useArrow) {
				startY = _size * 1.5;
				//上箭头
				if (!arrowUp) {
					arrowUp = new Sprite();
					arrowUp.mouseEnabled = true;
					addChild(arrowUp);
				}
				arrowUp.graphics.clear();
				arrowUp.graphics.beginFill(color, .5);
				arrowUp.graphics.moveTo(_size, 0);
				arrowUp.graphics.lineTo(_size * 2, _size);
				arrowUp.graphics.lineTo(0, _size);
				arrowUp.graphics.lineTo(_size, 0);
				
				if(!arrowUp.hasEventListener(MouseEvent.MOUSE_DOWN)){
					arrowUp.addEventListener(MouseEvent.MOUSE_DOWN, scrollUp);
					arrowUp.addEventListener(MouseEvent.CLICK, stopMouseEvent);
				}
				
				endY = rect.height - _size * 1.5;
				//下箭头
				if (!arrowDown) {
					arrowDown = new Sprite();
					arrowDown.mouseEnabled = true;
					addChild(arrowDown);
				}
				arrowDown.graphics.clear();
				arrowDown.graphics.beginFill(color, .5);
				arrowDown.graphics.moveTo(0, 0);
				arrowDown.graphics.lineTo(_size * 2, 0);
				arrowDown.graphics.lineTo(_size, _size);
				arrowDown.graphics.lineTo(0, 0);
				arrowDown.y = rect.height - _size;
				if(!arrowDown.hasEventListener(MouseEvent.MOUSE_DOWN)){
					arrowDown.addEventListener(MouseEvent.MOUSE_DOWN, scrollDown);
					arrowDown.addEventListener(MouseEvent.CLICK, stopMouseEvent);
				}
			}else {
				startY = 0;
				endY = rect.height;
			}
			
			//滚动背景
			this.graphics.clear();
			this.graphics.beginFill(color, .5);
			this.graphics.drawRoundRect(0, startY, _size * 2, endY - startY, _size * 2);
			this.graphics.endFill();
			
			if(!scroll){
				scroll = new Sprite();
				addChild(scroll);
				scroll.mouseEnabled = true;
				scroll.addEventListener(MouseEvent.MOUSE_DOWN, dragBarScroll);
				scroll.addEventListener(MouseEvent.CLICK, stopMouseEvent);
			}
			
			if (!this.hasEventListener(MouseEvent.CLICK)) {
				this.addEventListener(MouseEvent.CLICK, clickScroll);
			}
			update();
		}
		
		public function stopMouseEvent(e:MouseEvent):void {
			e.stopPropagation();
			e.preventDefault();
		}
		
		public function dragScroll(e:MouseEvent):void {
			var mx:Number = stage.mouseX, my:Number = stage.mouseY;
			var rect:Rectangle = target.scrollRect;
			var bx:Number = rect.x, by:Number = rect.y;
			var lastTime:Number = new Date().getTime();
			var speed:Number = 0;
			
			var moveHandle:Function=function(e:MouseEvent):void {
				var currentTime:Number= new Date().getTime();
				var cx:Number = e.stageX, cy:Number = e.stageY;
				var ty:Number = by - cy + my;
				speed = (currentTime-lastTime) / (cy - my);
				//trace('dragScroll',speed);
				//范围判断
				if (ty < 0 ) {
					ty = 0;
				}
				if (ty > targetHeight - rect.height ) {
					ty = targetHeight - rect.height;
				}
				rect.y = ty;
				target.scrollRect = rect;
				if (targetHeight == rect.height) {
					scroll.y = startY;
				}else{
					scroll.y = startY + (endY - startY - scroll.height) * ty / (targetHeight - rect.height);
				}
				lastTime = currentTime;
			};
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE, moveHandle);
			stage.addEventListener(MouseEvent.MOUSE_UP, function(e:MouseEvent):void {
				try {
					stage.removeEventListener(MouseEvent.MOUSE_MOVE, moveHandle);
					stage.removeEventListener(MouseEvent.MOUSE_UP, arguments.callee);
				}catch (e:Error) {
					trace('dragScroll',e );
				}
				if (targetHeight == rect.height) {
					scrollTo(0);
				}else{
					scrollTo((rect.y - Math.abs(speed) * (speed / 0.00005)) / (targetHeight - rect.height));
				}
			});
		}
		
		public function dragBarScroll(e:MouseEvent):void {
			var mx:Number = stage.mouseX, my:Number = stage.mouseY;
			var bx:Number = scroll.x, by:Number = scroll.y;
			var lastTime:Number = new Date().getTime();
			var speed:Number = 0;
			var moveHandle:Function=function(e:MouseEvent):void {
				var currentTime:Number= new Date().getTime();
				var cx:Number = e.stageX, cy:Number = e.stageY;
				var ty:Number = by + cy - my;
				speed = (currentTime-lastTime) / (cy - my);
				//trace('dragBarScroll',speed);
				//范围判断
				if (ty < startY ) {
					ty = startY;
				}
				if (ty > endY - scroll.height ) {
					ty = endY - scroll.height;
				}
				scroll.y = ty;
				var rect:Rectangle = target.scrollRect;
				if (endY - startY == scroll.height) {
					rect.y = 0;
				}else{
					rect.y = (targetHeight - rect.height) * (scroll.y - startY) / (endY - startY - scroll.height);
				}
				target.scrollRect = rect;
				lastTime = currentTime;
			};
			stage.addEventListener(MouseEvent.MOUSE_MOVE, moveHandle);
			stage.addEventListener(MouseEvent.MOUSE_UP, function(e:MouseEvent):void {
				try {
					stage.removeEventListener(MouseEvent.MOUSE_MOVE, moveHandle);
					stage.removeEventListener(MouseEvent.MOUSE_UP, arguments.callee);
				}catch (e:Error) {
					trace('dragBarScroll',e );
				}
				if (endY -startY == scroll.height) {
					scrollTo(0);
				}else{
					scrollTo((scroll.y - startY + Math.abs(speed) * (speed / 0.005)) / (endY - scroll.height));
				}
			});
		}
				
		//根据鼠标事件滚动到指定位置
		public function clickScroll(e:MouseEvent):void {
			scrollTo((e.localY - startY) / (endY - startY));
		}
		
		//给定比例滚动
		public function scrollTo(ratio:Number = 0, ease:Function=null):void {
			if (ratio < 0) {
				ratio = 0;
			}
			if (ratio > 1) {
				ratio = 1;
			}
			if (ease == null) {
				ease = 	Quint.easeOut;
			}
			
			if (scrollTween) {
				scrollTween.kill();
			}
			scrollTween = new TweenLite(scroll, .3, { y:startY + (endY - startY - scroll.height) * ratio,ease:ease } );
			var rect:Rectangle = target.scrollRect;
			
			if (targetTween) {
				targetTween.kill();
			}
			targetTween = new TweenLite(rect,.3,{ y:(targetHeight - rect.height) * ratio,ease:ease, onUpdate:function():void {
				target.scrollRect = rect;
				} });
		}
		
		//向上滚动
		public function scrollUp(e:MouseEvent):void {
			stopMouseEvent(e);
			var ty:Number = scroll.y;
			ty -= scroll.height * .8;
			//范围判断
			if (ty < startY ) {
				ty = startY;
			}
			scroll.y = ty;
			var rect:Rectangle = target.scrollRect;
			if (endY - startY == scroll.height) {
				rect.y = 0;
			}else{
				rect.y = (targetHeight - rect.height) * (scroll.y - startY) / (endY - startY - scroll.height);
			}
			target.scrollRect = rect;
		}
		
		//向下滚动
		public function scrollDown(e:MouseEvent):void {
			stopMouseEvent(e);
			var ty:Number = scroll.y;
			ty += scroll.height * .8;
			//范围判断
			if (ty > endY - scroll.height ) {
				ty = endY - scroll.height;
			}
			scroll.y = ty;
			var rect:Rectangle = target.scrollRect;
			rect.y = (targetHeight - rect.height) * (scroll.y - startY) / (endY - startY - scroll.height);
			target.scrollRect = rect;
		}
		
		//更新
		public function update(e:TimerEvent=null):void {
			var rect:Rectangle = target.scrollRect;
			var bound:Rectangle = Helper.getFullBounds(target);
			this.x = target.x + bound.width - this.width;
			this.y = target.y;
			if (bound.width == targetWidth && bound.height == targetHeight) {
				return;
			}
			//var oldy:Number = scroll.y;
			targetWidth = bound.width;
			targetHeight = bound.height;
			if (targetHeight < rect.height) {
				targetHeight = rect.height;
			}
			if (targetWidth < rect.width) {
				targetWidth = rect.width;
			}
			
			if (target.height <= 0) {
				this.visible = false;
				return;
			}else {
				this.visible = true;
			}
			var height:Number = (endY - startY) * (rect.height / targetHeight);
			if (height < 10) {
				height = 10;
			}
			if (targetHeight == rect.height) {
				scroll.y = startY;
			}else{
				scroll.y = startY + (endY - startY - height) * (rect.y / (targetHeight - rect.height));
			}
			
			scroll.graphics.clear();
			scroll.graphics.beginFill(color);
			scroll.graphics.drawRoundRect(0, 0, _size * 2, height, _size * 2);
			scroll.graphics.endFill();
		}
		
	}

}