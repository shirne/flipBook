package com.shirne.component 
{
	import com.greensock.easing.*;
	import com.greensock.TweenLite;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.events.TouchEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.html.HTMLLoader;
	import flash.utils.Timer;
	
	/**
	 * 针对HTMLLoader的滚动条
	 * @author Shirne http://www.shirne.com/
	 */
	public class HTMLScrollBar extends Sprite 
	{
		private var target:HTMLLoader;
		
		private var targetWidth:Number=0;
		private var targetHeight:Number = 0;
		
		private var scrollPercent:Number=0;
		
		private var useArrow:Boolean;
		
		private var arrowUp:Sprite;
		private var arrowDown:Sprite;
		private var scroll:Sprite;
		
		private var color:uint = 0x666666;
		
		private var _size:Number = 4;
		
		private var startY:Number
		
		private var endY:Number;
		
		private var scrollTween:TweenLite;
		private var targetTween:TweenLite;
		
		
		public function HTMLScrollBar(target:HTMLLoader, arrow:Boolean=true, refresh:Number=500) 
		{
			this.target = target;
			this.useArrow = arrow;
			
			this.target.parent.addChild(this);
			
			target.addEventListener(TouchEvent.TOUCH_BEGIN, dragScroll);
			//target.addEventListener(MouseEvent.MOUSE_DOWN, dragScroll);
			target.addEventListener(Event.SCROLL, updateScroll);
			
			target.addEventListener(Event.HTML_BOUNDS_CHANGE, update);
			target.addEventListener(Event.RESIZE, setStyle);
			setStyle();
			
			var isMove:Boolean = false;
			//兼容html内部事件
			target.addEventListener(Event.HTML_DOM_INITIALIZE, function(e:Event):void {
				
				var dom:*= target.window.document;
				
				//trace(isMove);
				dom.addEventListener("mousedown", function(e:*):void {
					//trace("mousedown",e.target);
					var downPoint:Point = new Point(e.pageX, e.pageY);
					var scrollV:Number = target.scrollV, scrollH:Number = target.scrollH;
					var moveHandle:Function=function(e:*):void {
						//trace("mousemove",e.target);
						target.scrollH = scrollH + (downPoint.x - e.pageX);
						target.scrollV = scrollV + (downPoint.y - e.pageY);
						updateScroll();
					};
					target.removeEventListener(Event.HTML_BOUNDS_CHANGE, update);
					dom.addEventListener("selectstart", stopall);
					dom.addEventListener("mousemove", moveHandle);
					dom.addEventListener("mouseup", function(e:*):void {
						//trace("mouseup",e.target);
						var upPoint:Point = new Point(e.pageX, e.pageY);
						if (Math.round(Point.distance(upPoint, downPoint))>5) {
							isMove = true;
						}
						dom.removeEventListener("mousemove", moveHandle);
						dom.removeEventListener("mouseup", arguments.callee);
						target.addEventListener(Event.HTML_BOUNDS_CHANGE, update);
						dom.removeEventListener("selectstart", stopall);
					});
				});
				
				
			});
			target.addEventListener(Event.COMPLETE, function(e:Event):void {
				var dom:*= target.window.document;
				var links:*= dom.getElementsByTagName('a');
				//trace(links,links.length)
				for (var j:int = 0; j < links.length; j++ ) {
					//trace(links[j])
					links[j].addEventListener("click", function(e:*):Boolean {
						//trace("aclick");
						if (isMove) {
							e.preventDefault();
							isMove = false;
							return false;
						}
						return true;
					})
				}
			});
		}
		
		//初始样式
		public function setStyle(e:Event=null):void {
			if (this.useArrow) {
				startY = _size * 1.5;
				//上箭头
				if (!arrowUp) {
					arrowUp = new Sprite();
					arrowUp.mouseEnabled = true;
					addChild(arrowUp);
				}
				arrowUp.graphics.clear();
				arrowUp.graphics.beginFill(color, .5);
				arrowUp.graphics.moveTo(_size, 0);
				arrowUp.graphics.lineTo(_size * 2, _size);
				arrowUp.graphics.lineTo(0, _size);
				arrowUp.graphics.lineTo(_size, 0);
				
				if(!arrowUp.hasEventListener(MouseEvent.MOUSE_DOWN)){
					arrowUp.addEventListener(MouseEvent.MOUSE_DOWN, scrollUp);
					arrowUp.addEventListener(MouseEvent.CLICK, stopMouseEvent);
				}
				
				endY = target.height - _size * 1.5;
				//下箭头
				if (!arrowDown) {
					arrowDown = new Sprite();
					arrowDown.mouseEnabled = true;
					addChild(arrowDown);
				}
				arrowDown.graphics.clear();
				arrowDown.graphics.beginFill(color, .5);
				arrowDown.graphics.moveTo(0, 0);
				arrowDown.graphics.lineTo(_size * 2, 0);
				arrowDown.graphics.lineTo(_size, _size);
				arrowDown.graphics.lineTo(0, 0);
				arrowDown.y = target.height - _size;
				if(!arrowDown.hasEventListener(MouseEvent.MOUSE_DOWN)){
					arrowDown.addEventListener(MouseEvent.MOUSE_DOWN, scrollDown);
					arrowDown.addEventListener(MouseEvent.CLICK, stopMouseEvent);
				}
			}else {
				startY = 0;
				endY = target.height;
			}
			
			//滚动背景
			this.graphics.clear();
			this.graphics.beginFill(color, .5);
			this.graphics.drawRoundRect(0, startY, _size * 2, endY - startY, _size * 2);
			this.graphics.endFill();
			
			if(!scroll){
				scroll = new Sprite();
				addChild(scroll);
				scroll.mouseEnabled = true;
				//scroll.addEventListener(TouchEvent.TOUCH_BEGIN, dragBarScroll);
				scroll.addEventListener(MouseEvent.MOUSE_DOWN, dragBarScroll);
				scroll.addEventListener(MouseEvent.CLICK, stopMouseEvent);
			}
			
			if (!this.hasEventListener(MouseEvent.CLICK)) {
				this.addEventListener(MouseEvent.CLICK, clickScroll);
			}
			update();
		}
		
		public function stopall(e:*):Boolean {
			e.preventDefault();
			e.stopPropagation();
			return false;
		}
		
		public function stopMouseEvent(e:MouseEvent):void {
			e.stopPropagation();
			e.preventDefault();
		}
		
		private var mx:Number;
		private var my:Number;
		private var bx:Number;
		private var by:Number;
		private var lastTime:Number;
		private var speed:Number;
		
		public function dragScroll(e:MouseEvent):void {
			mx = stage.mouseX;
			my = stage.mouseY;
			bx = target.scrollH;
			by = target.scrollV;
			lastTime = new Date().getTime();
			speed = 0;
			//trace(e);
			//trace(e.target);
			//stage.addEventListener(MouseEvent.MOUSE_MOVE, stageMove);
			//stage.addEventListener(MouseEvent.MOUSE_UP, stageUp);
			stage.addEventListener(TouchEvent.TOUCH_BEGIN, touchMove);
			stage.addEventListener(TouchEvent.TOUCH_END, touchUp);
		}
		
		private function touchMove(e:TouchEvent):void {
			var currentTime:Number= new Date().getTime();
			var cx:Number = e.stageX, cy:Number = e.stageY;
			var ty:Number = by - cy + my;
			speed = (currentTime-lastTime) / (cy - my);
			
			//范围判断
			if (ty < 0 ) {
				ty = 0;
			}
			if (ty > targetHeight - target.height ) {
				ty = targetHeight - target.height;
			}
			target.scrollV = ty;
			if (targetHeight == target.height) {
				scroll.y = startY;
			}else{
				scroll.y = startY + (endY - startY - scroll.height) * ty / (targetHeight - target.height);
			}
			lastTime = currentTime;
		}
		
		private function touchUp(e:TouchEvent):void {
			try {
				stage.removeEventListener(TouchEvent.TOUCH_MOVE, touchMove);
				stage.removeEventListener(TouchEvent.TOUCH_END, touchUp);
			}catch (e:Error) {
				trace('dragScroll',e );
			}
			if (targetHeight == target.height) {
				scrollTo(0);
			}else{
				scrollTo((target.scrollV - Math.abs(speed) * (speed / 0.00005)) / (targetHeight - target.height));
			}
		}
		
		public function dragBarScroll(e:MouseEvent):void {
			var mx:Number = stage.mouseX, my:Number = stage.mouseY;
			var bx:Number = scroll.x, by:Number = scroll.y;
			var lastTime:Number = new Date().getTime();
			var speed:Number = 0;
			var moveHandle:Function=function(e:MouseEvent):void {
				var currentTime:Number= new Date().getTime();
				var cx:Number = e.stageX, cy:Number = e.stageY;
				var ty:Number = by + cy - my;
				speed = (currentTime-lastTime) / (cy - my);
				
				//范围判断
				if (ty < startY ) {
					ty = startY;
				}
				if (ty > endY - scroll.height ) {
					ty = endY - scroll.height;
				}
				scroll.y = ty;
				if (endY - startY == scroll.height) {
					target.scrollV = 0;
				}else{
					target.scrollV = (targetHeight - target.height) * (scroll.y - startY) / (endY - startY - scroll.height);
				}
				lastTime = currentTime;
			};
			stage.addEventListener(MouseEvent.MOUSE_MOVE, moveHandle);
			stage.addEventListener(MouseEvent.MOUSE_UP, function(e:MouseEvent):void {
				try {
					stage.removeEventListener(MouseEvent.MOUSE_MOVE, moveHandle);
					stage.removeEventListener(MouseEvent.MOUSE_UP, arguments.callee);
				}catch (e:Error) {
					trace('dragBarScroll',e );
				}
				if (endY - startY == scroll.height) {
					scrollTo(0);
				}else{
					scrollTo((scroll.y - startY + Math.abs(speed) * (speed / 0.005)) / (endY - scroll.height));
				}
			});
		}
				
		//根据鼠标事件滚动到指定位置
		public function clickScroll(e:MouseEvent):void {
			scrollTo((e.localY - startY) / (endY - startY));
		}
		
		//给定比例滚动
		public function scrollTo(ratio:Number = 0, ease:Function=null):void {
			if (ratio < 0) {
				ratio = 0;
			}
			if (ratio > 1) {
				ratio = 1;
			}
			if (ease == null) {
				ease = 	Quint.easeOut;
			}
			
			if (scrollTween) {
				scrollTween.kill();
			}
			scrollTween = new TweenLite(scroll, .3, { y:startY + (endY - startY - scroll.height) * ratio,ease:ease } );
			var rect:Rectangle = target.scrollRect;
			
			if (targetTween) {
				targetTween.kill();
			}
			targetTween = new TweenLite(target,.3,{ scrollV:(targetHeight - target.height) * ratio,ease:ease });
		}
		
		//向上滚动
		public function scrollUp(e:MouseEvent):void {
			stopMouseEvent(e);
			var ty:Number = scroll.y;
			ty -= scroll.height * .8;
			//范围判断
			if (ty < startY ) {
				ty = startY;
			}
			scroll.y = ty;
			if (endY - startY == scroll.height) {
				target.scrollV = 0;
			}else{
				target.scrollV = (targetHeight - target.height) * (scroll.y - startY) / (endY - startY - scroll.height);
			}
		}
		
		//向下滚动
		public function scrollDown(e:MouseEvent):void {
			stopMouseEvent(e);
			var ty:Number = scroll.y;
			ty += scroll.height * .8;
			//范围判断
			if (ty > endY - scroll.height ) {
				ty = endY - scroll.height;
			}
			scroll.y = ty;
			if (endY - startY == scroll.height) {
				target.scrollV = 0;
			}else{
				target.scrollV = (targetHeight - target.height) * (scroll.y - startY) / (endY - startY - scroll.height);
			}
		}
		
		//更新
		public function updateScroll(e:Event=null):void {
			if (targetHeight == target.height) {
				scroll.y = startY;
			}else{
				scroll.y = startY + (endY - startY - scroll.height) * (target.scrollV / (targetHeight - target.height));
			}
		}
		
		//更新
		public function update(e:Event = null):void {
			this.x = target.x + target.width - this.width;
			this.y = target.y;
			if (target.contentWidth == targetWidth && target.contentHeight == targetHeight) {
				return;
			}
			targetWidth = target.contentWidth;
			targetHeight = target.contentHeight;
			if (targetHeight < target.height) {
				targetHeight = target.height;
			}
			if (targetWidth < target.width) {
				targetWidth = target.width;
			}
			
			if (target.height <= 0) {
				this.visible = false;
				return;
			}else {
				this.visible = true;
			}
			var height:Number = (endY - startY) * (target.height / targetHeight);
			if (height < 10) {
				height = 10;
			}
			if (targetHeight == target.height) {
				scroll.y = startY;
			}else{
				scroll.y = startY + (endY - startY - height) * (target.scrollV / (targetHeight - target.height));
			}
			
			scroll.graphics.clear();
			scroll.graphics.beginFill(color);
			scroll.graphics.drawRoundRect(0, 0, _size * 2, height, _size * 2);
			scroll.graphics.endFill();
		}
		
	}

}