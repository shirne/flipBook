package com.shirne.component 
{
	import com.greensock.TweenNano;
	import flash.display.DisplayObjectContainer;
	import flash.display.GradientType;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.geom.Matrix;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.Timer;
	
	/**
	 * 全局提示管理器
	 * @author Shirne
	 */
	public class Tip extends Object 
	{
		public static var tip:Sprite;
		
		public static var tipMask:Sprite;
		
		public static var tipBox:Sprite;
		
		//图标
		public static var tipIco:TipIco;
		
		public static var tipText:TextField;
		
		//遮罩配置
		public static var useMask:Boolean;
		public static var timeOut:Number;
		
		//全局配置
		public static var maskColor:uint;
		public static var boxColor:uint;
		public static var boxBorder:uint;
		public static var textColor:uint;
		
		//计时
		public static var timer:Timer;
		
		public function Tip() :void
		{
			
		}
		
		public static function init(container:DisplayObjectContainer, maskcolor:uint=0x000000, boxcolor:uint=0x111111, boxborder:uint=0x333333, textcolor:uint=0xaaaaaa):void {
			maskColor = maskcolor;
			boxColor = boxcolor;
			boxBorder = boxborder;
			textColor = textcolor;
			
			tip = new Sprite();
			
			tipMask = new Sprite();
			tip.addChild(tipMask);
			
			tipBox = new Sprite();
			tipText = new TextField();
			tipText.autoSize = TextFieldAutoSize.LEFT;
			var tFmt:TextFormat = new TextFormat('微软雅黑', 14, textColor);
			tFmt.letterSpacing = 1;
			tipText.defaultTextFormat = tFmt;
			tipText.x = 32;
			tipText.y = 3;
			
			tip.addChild(tipBox);
			tipBox.addChild(tipText);
			
			tipIco = new TipIco();
			tipIco.x = 18;
			tipIco.y = 16;
			tipBox.addChild(tipIco);
			
			container.addChild(tip);
			timer = new Timer(10, 1);
			timer.addEventListener(TimerEvent.TIMER, close);
			
			tip.alpha = 0;
		}
		
		//显示或更新
		public static function show(message:String, timeout:Number = 0, ico:String='loading', usemask:Boolean = true):void {
			tip.visible = true;
			useMask = usemask;
			timeOut = timeout;
			
			if (!useMask) {
				tipMask.graphics.clear();
				tipMask.visible = false;
			}else {
				tipMask.visible = true;
			}
			
			tip.parent.setChildIndex(tip, tip.parent.numChildren - 1);
			
			tipIco.setStyle(ico);
			
			tipText.text = message;
			var mtx:Matrix = new Matrix();
			mtx.createGradientBox(tipText.width + 20 +25, tipText.height + 10, Math.PI * .5);
			tipBox.graphics.clear();
			tipBox.graphics.lineStyle(2, boxBorder);
			tipBox.graphics.lineGradientStyle(GradientType.LINEAR,
							[Helper.colorTrans(boxBorder, 2), Helper.colorTrans(boxBorder, 1.2), Helper.colorTrans(boxBorder, .8), Helper.colorTrans(boxBorder, .8)],
							[1, 1, 1, 1], [0, 140, 142, 255], mtx);
			tipBox.graphics.beginGradientFill(GradientType.LINEAR,
							[Helper.colorTrans(boxColor, 2), Helper.colorTrans(boxColor, 1.1), Helper.colorTrans(boxColor, .1), Helper.colorTrans(boxColor, .1)],
							[1, 1, 1, 1], [0, 140, 142, 255], mtx);
			tipBox.graphics.drawRect(0, 0, tipText.width + 20 +25, tipText.height + 10);
			tipBox.graphics.endFill();
			
			resizer();
			tip.stage.addEventListener(Event.RESIZE, resizer);
			var t:TweenNano = TweenNano.to(tip, .3, { alpha:1, onComplete:function():void {
				t.kill();
			}});
			if (timeOut > 0) {
				timer.delay = timeOut * 1000;
				timer.start();
			}
		}
		
		//关闭
		public static function close(speed:Number = .3):void {
			var t:TweenNano = TweenNano.to(tip, .5, { alpha:0, onComplete:function():void {
				tip.visible = false;
				t.kill();
			}});
			timer.reset();
			tipIco.reset();
			
			tip.stage.removeEventListener(Event.RESIZE, resizer);
		}
		
		//定位
		public static function resizer(e:Event = null):void {
			if (useMask) {
				tipMask.graphics.clear();
				tipMask.graphics.beginFill(maskColor, .5);
				tipMask.graphics.drawRect(0, 0, tip.stage.stageWidth, tip.stage.stageHeight);
				tipMask.graphics.endFill();
			}
			tipBox.x = (tip.stage.stageWidth - tipBox.width) * .5;
			tipBox.y = (tip.stage.stageHeight - tipBox.height) * .5;
		}
		
	}

}