package com.shirne.component 
{
	import flash.display.BitmapData;
	import flash.display.GradientType;
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.geom.Matrix;
	import flash.utils.Timer;
	
	/**
	 * 提示层图标
	 * @author Shirne http://www.shirne.com/
	 */
	public class TipIco extends Sprite 
	{
		//图标常量
		public static const LOADING:String = "loading";
		public static const OK:String = "ok";
		public static const ERROR:String = "error";
		public static const WARNING:String = "warning";
		
		[Embed(source = "/../lib/ok.png")]
		public static var okBMPClass:Class;
		public static var okBmp:BitmapData;
		
		[Embed(source = "/../lib/warning.png")]
		public static var warningBMPClass:Class;
		public static var warningBmp:BitmapData;
		
		[Embed(source = "/../lib/error.png")]
		public static var errorBMPClass:Class;
		public static var errorBmp:BitmapData;
		
		private var timer:Timer
		private var mtxRotat:Number = 0;
		private var size:Number = 10;
		
		public function TipIco() 
		{
			if (!okBmp) {
				okBmp = new okBMPClass().bitmapData;
				warningBmp = new warningBMPClass().bitmapData;
				errorBmp = new errorBMPClass().bitmapData;
			}
			
			timer = new Timer(100);
			timer.addEventListener(TimerEvent.TIMER, timing);
		}
		
		public function reset():void {
			timer.reset();
		}
		
		public function setStyle(style:String):void {
			timer.reset();
			if (this['ico_'+style]) {
				this['ico_'+style]();
			}
		}
		
		private function ico_loading():void {
			timer.start();
		}
		
		private function ico_ok():void {
			var mtx:Matrix = new Matrix();
			this.graphics.clear();
			this.graphics.beginBitmapFill(okBmp,new Matrix(1,0,0,1,-size,-size),false);
			this.graphics.drawRect(-size, -size, size * 2, size * 2);
			this.graphics.endFill();
		}
		
		private function ico_error():void {
			this.graphics.clear();
			this.graphics.beginBitmapFill(errorBmp,new Matrix(1,0,0,1,-size,-size),false);
			this.graphics.drawRect(-size, -size, size * 2, size * 2);
			this.graphics.endFill();
		}
		
		private function ico_warning():void {
			this.graphics.clear();
			this.graphics.beginBitmapFill(warningBmp,new Matrix(1,0,0,1,-size,-size),false);
			this.graphics.drawRect(-size, -size, size * 2, size * 2);
			this.graphics.endFill();
		}
		
		public function timing(e:TimerEvent):void {
			mtxRotat += 20;
			if (mtxRotat > 360) {
				mtxRotat = 0;
			}
			
			this.graphics.clear();
			Helper.drawCircle(this.graphics, size, [0xffffff, 0xffffff], [0, 1], [0, 255], 360, mtxRotat, size-2);
			
		}
		
	}

}