﻿package com.shirne.component {
	import flash.ui.ContextMenu;
	import flash.display.InteractiveObject ;
	import flash.ui.ContextMenuItem;
	import flash.events.ContextMenuEvent;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	
	public class FixedMenu extends Object {

		private var myMenu:ContextMenu=null;
		
		public var context:InteractiveObject =null;
		
		public function FixedMenu(context:InteractiveObject,text:String="", url:String=""):void {
			this.context = context;
			myMenu = new ContextMenu();
			myMenu.hideBuiltInItems();
			if(text != ""){
				addItem(text, url);
			}
		}
		
		public function addItem(text:String, url:String=""):void{
			var menu:ContextMenuItem=new ContextMenuItem(text);
			if(url != ""){
				menu.addEventListener(ContextMenuEvent.MENU_ITEM_SELECT,function(e:ContextMenuEvent):void{
					navigateToURL(new URLRequest(url));
				});
			}
			myMenu.customItems.push(menu);
			this.context.contextMenu=myMenu;
		}

	}
	
}
