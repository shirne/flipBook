package com.shirne.component 
{
	import com.greensock.TweenNano;
	import com.txmdtea.TSDQSClient.DataManager;
	import com.txmdtea.TSDQSClient.events.ImageEvent;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.PixelSnapping;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.utils.ByteArray;
	
	/**
	 * 加载本地图片
	 * @author Shirne
	 */
	public class LocalImage extends Sprite 
	{
		private var _src:String;
		private var _bmp:Bitmap;
		
		private var _width:Number;
		private var _height:Number;
		
		private var originalwidth:Number=-1;
		private var originalheight:Number=-1;
		
		private var act:Shape;
		
		public var data:Object=new Object();
		
		public override function get width():Number {
			return _width;
		}
		public override function get height():Number {
			return _height;
		}
		
		public override function set width(w:Number):void {
			_width = w;
			resizeTo(_width, _height);
		}
		
		public override function set height(h:Number):void {
			_height = h;
			resizeTo(_width, _height);
		}
		
		public function get originalWidth():Number {
			return originalwidth;
		}
		public function get originalHeight():Number {
			return originalheight;
		}
		
		public function resizeTo(w:Number = -1, h:Number = -1):void {
			if (w != -1) {
				this._width = w;
			}
			if (h != -1) {
				this._height = h;
			}
			if (!_bmp) {
				return;
			}
			var scale:Number = Math.min(_width / originalwidth, _height / originalheight);
			if (scale > 0) {
				_bmp.scaleX = _bmp.scaleY = scale;
			}
			if (_width < 0) {
				_width = _bmp.width;
			}
			if (_height < 0) {
				_height = _bmp.height;
			}
			_bmp.x = (_width - _bmp.width) * .5;
			_bmp.y = (_height - _bmp.height) * .5;
			deActive();
		}
		
		public function get bitmap():Bitmap {
			return _bmp;
		}
		
		[Event(name="onload", type="com.txmdtea.TSDQSClient.events.ImageEvent")]
		public function LocalImage(src:String, w:Number =-1,h:Number=-1, dat:Object=null ) 
		{
			this._src = src;
			this._width = w;
			this._height = h;
			if (dat) {
				this.data = dat;
			}
			this.addEventListener(Event.ADDED_TO_STAGE, initialize);
			this.addEventListener(Event.REMOVED_FROM_STAGE, terminate);
		}
		
		public function initialize(e:Event):void {
			this.removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			act = new Shape();
			addChild(act);
			
			var self:Image = this;
			loadImage(_src, function(bmpd:BitmapData):void {
				originalwidth = bmpd.width;
				originalheight = bmpd.height;
				_bmp = new Bitmap(bmpd, PixelSnapping.ALWAYS, true);
				var scale:Number = -1;
				if (_width > 0 && _height > 0) {
					scale = Math.min(_width / originalwidth, _height / originalheight);
				}else if (_width > 0) {
					scale = _width / originalwidth;
				}else if (_height > 0) {
					scale = _height / originalheight;
				}
				
				if (scale > 0) {
					_bmp.scaleX = _bmp.scaleY = scale;
				}
				if (_width < 0) {
					_width = _bmp.width;
				}
				if (_height < 0) {
					_height = _bmp.height;
				}
				_bmp.smoothing = true;
				_bmp.x = (_width - _bmp.width) * .5;
				_bmp.y = (_height - _bmp.height) * .5;
				addChild(_bmp);
				setChildIndex(_bmp, 0);
				deActive();
				self.dispatchEvent(new ImageEvent(ImageEvent.ONLOAD));
			});
		}
		
		/**
		 * 
		 * @param	src			图片路径
		 * @param	callBack	回调函数,接收一个bitmapdata对象
		 */
		public static function loadImage(src:String, callBack:Function):void {
			var fs:FileStream = new FileStream();
			fs.addEventListener(Event.COMPLETE, function():void {
				var ldr:Loader = new Loader();
				ldr.contentLoaderInfo.addEventListener(Event.COMPLETE, function(e:Event):void {
					var bmp:BitmapData = new BitmapData(ldr.content.width, ldr.content.height,true, 0x00000000);
					bmp.draw(ldr.content);
					ldr.unload();
					callBack(bmp);
				});
				ldr.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, function(e:IOErrorEvent):void { } );
				var bytes:ByteArray = new ByteArray();
				fs.readBytes(bytes);
				fs.close();
				ldr.loadBytes(bytes);
			});
			fs.addEventListener(IOErrorEvent.IO_ERROR, function(e:IOErrorEvent):void {
				DataManager.log(new IOError(e.toString()));
			});
			fs.openAsync(File.applicationDirectory.resolvePath(src), FileMode.READ);
		}
		
		public function active():void {
			if(this.numChildren>0){
				setChildIndex(act, this.numChildren - 1);
			}
			act.graphics.clear();
			act.graphics.lineStyle(2, 0xff6600);
			act.graphics.drawRect(0, 0, _width, _height);
		}
		
		public function deActive():void {
			if(this.numChildren>0){
				setChildIndex(act, this.numChildren - 1);
			}
			act.graphics.clear();
			act.graphics.lineStyle(2, 0xffffff, .3);
			act.graphics.drawRect(0, 0, _width, _height);
		}
		
		public function terminate(e:Event):void {
			this.addEventListener(Event.ADDED_TO_STAGE, initialize);
			if (_bmp) {
				_bmp.bitmapData.dispose();
			}
			while (this.numChildren > 0) {
				removeChildAt(0);
			}
		}
		
	}

}