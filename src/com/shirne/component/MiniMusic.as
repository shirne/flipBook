﻿package com.shirne.component {
	import flash.display.Sprite;
	import flash.media.SoundTransform;
	import flash.media.Sound;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.ProgressEvent;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import flash.media.SoundMixer;
	import flash.media.SoundChannel;
	import flash.utils.ByteArray;
	import flash.display.Graphics;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
	public class MiniMusic extends Sprite {
		private var list:Array=[];
		private var index:int=0;
		
		private var timer:Timer=null;
		private var chars:Sprite=null;
		private var spectrum:ByteArray=null;
		private var blockCount:int = 8;
		private var blockWidth:Number = 3.5;
		private var blockSpace:Number = 1.5;
		
		//线条与间距比例
		private var blockRatio:Number=1.5;
		
		private var bgcolor:uint=0x000000;
		private var bgalpha:Number=.4;
		private var color:uint=0xffffff;
		private var calpha:Number=.8;
		private var cWidth:int=40;
		private var cHeight:int=20;
		
		//声音
		private var sound:Sound=null;
		private var sp:Number=0;
		//声道
		private var sc:SoundChannel=null;
		//音量控制
		private var st:SoundTransform=null;
		//确定是否因安全限制而无法访问任何声音
		private var isDync:Boolean=false;
		
		private var frames:Array=[];
		private var frameIndex:int=0;
		
		//0-停止,1-暂停,2-播放
		private var state:int=0;
		
		private var isLoading:Boolean=false;
		
		
		public function MiniMusic(cfg) :void{
			var itm:XML=null;
			for(var i:int=0;i<cfg.item.length();i++){
				itm=cfg.item[i];
				list.push({
						  name:itm.name,
						  url:itm.url
						  });
			}
			if(String(cfg.width)){
				cWidth = int(cfg.width);
			}
			if(String(cfg.height)){
				cHeight = int(cfg.height);
			}
			if(String(cfg.bgcolor)){
				bgcolor = int(cfg.bgcolor);
			}
			if(String(cfg.color)){
				color = int(cfg.color);
			}
			if(String(cfg.bgalpha)){
				bgalpha = Number(cfg.bgalpha);
			}
			if(String(cfg.color)){
				calpha = Number(cfg.alpha);
			}
			if(String(cfg.block.count)){
				blockCount = int(cfg.block.count);
			}
			if(String(cfg.block.ratio)){
				blockRatio = Number(cfg.block.ratio);
			}
			blockSpace = cWidth/(blockCount*(blockRatio+1)-1);
			blockWidth = blockSpace * blockRatio;
			addEventListener(Event.ADDED_TO_STAGE,init);
			addEventListener(Event.REMOVED_FROM_STAGE,term);
		}
		//初始化
		public function init(e:Event=null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE,init);
			//创建背景
			graphics.beginFill(bgcolor,bgalpha)
			graphics.drawRect(0,0,cWidth,cHeight);
			graphics.endFill();
			
			//创建图谱
			chars=new Sprite();
			addChild(chars);
			
			isDync = SoundMixer.areSoundsInaccessible();
			var firstFrame:Array=new Array(blockCount);
			if(frames.length<1){
				frames.push(new Array(blockCount));
				for(var i:int=0;i<blockCount;i++){
					frames[0][i]=int(Math.random()*cHeight);
				}
			}
			if(isDync){
				timer=new Timer(50);
				if(frames.length<2){
					for(var j:int=1;j<10;j++){
						frames.push(new Array(blockCount));
						for(i=0;i<blockCount;i++){
							frames[j][i]=int(Math.random()*cHeight);
						}
					}
				}
				timer.addEventListener(TimerEvent.TIMER,showChar);
			}else{
				timer=new Timer(150);
				timer.addEventListener(TimerEvent.TIMER,drawChar);
			}
			
			showChar();
			if(list.length < 1)return;
			
			//创建可点击遮罩
			var clickLayer:Sprite=new Sprite();
			clickLayer.graphics.beginFill(0x000000,0);
			clickLayer.graphics.drawRect(0,0,cWidth,cHeight);
			clickLayer.graphics.endFill();
			
			clickLayer.addEventListener(MouseEvent.CLICK,changeState);
			clickLayer.buttonMode=true;
			
			addChild(clickLayer);
			
			spectrum = new ByteArray();
			playSound(index);
		}
		
		public function term(e:Event):void
		{
			sc.stop();
			timer.stop();
			timer.removeEventListener(TimerEvent.TIMER,drawChar);
			timer.removeEventListener(TimerEvent.TIMER,showChar);
			graphics.clear();
			while(numChildren>0){
				removeChildAt(0);
			}
			sp=0;
			addEventListener(Event.ADDED_TO_STAGE,init);
		}
		//点击改变播放状态
		public function changeState(e:Event):void
		{
			if(sc)var sp=sc.position;
			if(state==2){
				state = 1
				sc.stop();
				//trace('stop')
			}else{
				state = 2;
				sc = sound.play(sp);
				//trace('play')
			}
		}
		
		public function playNext(e:Event):void
		{
			playSound();
		}
		
		public function playSound(idx:int=-2):void
		{
			
			if(idx==-2){
				idx = index+1;
			}
			if(idx >= list.length)idx = 0;
			if(idx < 0)idx = list.length-1;
			index = idx;
			//trace('play',index,list.length);
			if(sound){
				if(sc){
					if(sc.hasEventListener(Event.SOUND_COMPLETE)){
						sc.removeEventListener(Event.SOUND_COMPLETE,playNext);
					}
					sc.stop();
				}
				if(isLoading){
					sound.close();
					isLoading=false;
				}
				if(sound.hasEventListener(ProgressEvent.PROGRESS))
					sound.removeEventListener(ProgressEvent.PROGRESS,loading);
				if(sound.hasEventListener(Event.COMPLETE))
					sound.removeEventListener(Event.COMPLETE,loadComplete);
				if(sound.hasEventListener(Event.ID3))
					sound.removeEventListener(Event.ID3, startPlay);
				if(sound.hasEventListener(IOErrorEvent.IO_ERROR))
					sound.removeEventListener(IOErrorEvent.IO_ERROR,loadError);
			}
			sp = 0;
			sound=new Sound(new URLRequest(list[idx].url));
			sound.addEventListener(ProgressEvent.PROGRESS,loading);
			sound.addEventListener(Event.COMPLETE,loadComplete);
			sound.addEventListener(Event.ID3, startPlay);
			sound.addEventListener(IOErrorEvent.IO_ERROR,loadError);
		}
		
		public function loading(e:ProgressEvent):void
		{
			//trace(e.bytesLoaded,e.bytesTotal);
			isLoading=true;
		}
		
		public function loadComplete(e:Event=null):void
		{
			//加载完成
			//trace("complete");
			sound.removeEventListener(ProgressEvent.PROGRESS,loading);
			sound.removeEventListener(Event.COMPLETE,loadComplete);
			isLoading=false;
		}
		
		public function startPlay(e:Event=null):void
		{
			//播放,并没有加载完成
			//trace("hadID3");
			sound.removeEventListener(Event.ID3, startPlay);
			state=2;
			sp = 0
			sc = sound.play(sp);
			sc.addEventListener(Event.SOUND_COMPLETE,playNext);
			timer.start();
		}
		
		public function loadError(e:Event=null):void
		{
			//trace('error');
			sound.removeEventListener(IOErrorEvent.IO_ERROR,loadError);
			//出错加载下一首
			playSound(index+1);
		}
		
		//画模拟的音频频谱
		public function showChar(e:Event=null):void
		{
			frameIndex = frameIndex>=frames.length-1?0:frameIndex+1;
			var frame:Array=frames[frameIndex];
			var g:Graphics=chars.graphics;
			g.clear();
			for(var i:int=0;i<frame.length;i++){
				g.beginFill(color,calpha);
				g.drawRect(i*(blockWidth+blockSpace),cHeight-frame[i],blockWidth,frame[i]);
				g.endFill();
			}
		}
		
		//画真实的音频频谱
		public function drawChar(e:Event=null):void
		{
			SoundMixer.computeSpectrum(spectrum,false,1);
			
			var CHANNEL_LENGTH:int=256;
			var g:Graphics=chars.graphics;
			g.clear();
			var sum:int=0;
			var step:int=int(CHANNEL_LENGTH/blockCount);
			for(var i:int=0;i<CHANNEL_LENGTH;i++){
				sum += (spectrum.readFloat()+1) * cHeight * .5;
				if(i % step==step-1){	//计算该段的平均频谱
					sum = int(sum/step);
					g.beginFill(color,calpha);
					g.drawRect(int(i/step)*(blockWidth+blockSpace),cHeight-sum,blockWidth,sum);
					g.endFill();
					sum=0;
				}
			}
		}

	}
	
}
