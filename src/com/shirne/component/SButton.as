﻿package com.shirne.component {
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.events.Event;
	import flash.text.TextFieldAutoSize;
	
	public class SButton extends Sprite {
		
		private var text:String="";
		private var textField:TextField=null;
		private var pad:int;

		public function SButton(txt:String="", pad:int=5) {
			this.buttonMode=true;
			this.textField=new TextField();
			this.textField.autoSize=TextFieldAutoSize.LEFT;
			this.pad = pad;
			if(txt){
				this.setText(txt);
			}
			this.addEventListener(Event.ADDED_TO_STAGE,initialize);
		}
		
		public function set setText(txt:String):void{
			this.text = txt;
			this.textField.text = this.text;
		}
		
		public function initialize(e:Event):void{
			this.removeEventListener(Event.ADDED_TO_STAGE,initialize);
		}

	}
	
}
