package com.shirne.component 
{
	import com.greensock.easing.Quart;
	import com.greensock.TweenNano;
	import com.txmdtea.TSDQSClient.events.ImageEvent;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.events.TouchEvent;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	import flash.geom.Transform;
	import flash.utils.Timer;
	
	/**
	 * 图片展示
	 * @author Shirne
	 */
	public class AlbumPane extends Sprite 
	{
		//全局尺寸
		private var aWidth:Number;
		private var aHeight:Number;
		
		//展示区域尺寸
		private var sWidth:Number;
		private var sHeight:Number;

		private var space:Number;
		
		private var photos:Array;
		private var texts:Array;
		
		private var align:String;
		
		private var sceneImg:Bitmap;
		private var sceneBox:Sprite;
		
		//左侧及右侧的图片,用于拖动切换
		private var leftBox:Sprite;
		private var leftImg:Bitmap;
		private var rightBox:Sprite;
		private var rightImg:Bitmap;
		
		//展示区域
		private var scene:Sprite;
		//列表区域
		private var list:Sprite;
		
		private var listBox:Sprite;
		
		private var listBoxWidth:Number = 0;
		private var listBoxHeight:Number = 0;
		//左右上下按钮
		private var leftBtn:SimpleButton;
		private var rightBtn:SimpleButton;
		private var topBtn:SimpleButton;
		private var bottomBtn:SimpleButton;
		
		//布局,缩略图在上下左右
		public static var BOTTOM:String = "bottom";
		public static var TOP:String = "top";
		public static var LEFT:String = "left";
		public static var RIGHT:String = "right";
		
		[Embed(source = "/com/shirne/lib/scroll.png")]
		public static var scrollBmp:Class;
		[Embed(source = "/com/shirne/lib/scrollActive.png")]
		public static var scrollActiveBmp:Class;
		
		public static var scrollData:BitmapData;
		
		public static var scrollActiveData:BitmapData;
		
		scrollData = new scrollBmp().bitmapData;
		scrollActiveData = new scrollActiveBmp().bitmapData;
		
		
		
		private var timer:Timer;
		private var timerHandler:Function;
		
		private var activeImg:Image;

		
		public override function get width():Number {
			return aWidth;
		}
		
		public override function get height():Number {
			return aHeight;
		}
		
		/**
		 * 
		 * @param	pics	图片路径
		 * @param	w		全局宽度
		 * @param	h		全局高度
		 */
		public function AlbumPane(pics:Array, w:Number, h:Number, sw:Number, sh:Number, align:String="bottom", space:Number=5):void 
		{
			this.texts = [];
			this.photos = [];
			pics.forEach(function(item:*, index:int, array:Array):void {
				var url:String,content:String='';
				if (item is String) {
					url = String(item);
				}else {
					if(item){
						url = String(item.picture);
						if (item.content) {
							content = String(item.content);
						}
					}
				}
				if (url && url.replace(/\s*/g) != '') {
					photos.push(url);
					texts.push(content);
				}
			});

			this.aWidth = w;
			this.aHeight = h;
			this.sWidth = sw;
			this.sHeight = sh;
			this.align = align;
			this.space = space;
			
			this.addEventListener(Event.ADDED_TO_STAGE, initialize);
			this.addEventListener(Event.REMOVED_FROM_STAGE, terminate);
		}
		
		public function setPhoto(pics:Array):void {
			return resetPhoto(pics);
		}
		
		public function initialize(e:Event = null):void {
			this.removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			scene = new Sprite();
			scene.scrollRect = new Rectangle(0,0,sWidth,sHeight);
			addChild(scene);
			
			list = new Sprite();
			addChild(list);
			listBox = new Sprite();
			list.addChild(listBox);
			
			var normalLeftStyle:Bitmap = new Bitmap(scrollData);
			var activeLeftStyle:Bitmap = new Bitmap(scrollActiveData);
			leftBtn = new SimpleButton(normalLeftStyle, normalLeftStyle, activeLeftStyle, normalLeftStyle);
			var normalRightStyle:Bitmap = new Bitmap(scrollData);
			var activeRightStyle:Bitmap = new Bitmap(scrollActiveData);
			var mtx:Matrix = new Matrix( -1, 0, 0, 1, normalRightStyle.width);
			normalRightStyle.transform.matrix = mtx;
			activeRightStyle.transform.matrix = mtx;
			rightBtn = new SimpleButton(normalRightStyle, normalRightStyle, activeRightStyle, normalRightStyle);
			var normalTopStyle:Bitmap = new Bitmap(scrollData);
			var activeTopStyle:Bitmap = new Bitmap(scrollActiveData);
			mtx = new Matrix(Math.cos(Math.PI*.5),-Math.sin(Math.PI*.5),Math.sin(Math.PI*.5),Math.cos(Math.PI*.5));
			normalTopStyle.transform.matrix = mtx;
			activeTopStyle.transform.matrix = mtx;
			topBtn = new SimpleButton(normalTopStyle,normalTopStyle,activeTopStyle,normalTopStyle);
			var normalBottomStyle:Bitmap = new Bitmap(scrollData);
			var activeBottomStyle:Bitmap = new Bitmap(scrollActiveData);
			mtx = new Matrix(Math.cos(-Math.PI*.5),-Math.sin(-Math.PI*.5),Math.sin(-Math.PI*.5),Math.cos(-Math.PI*.5));
			normalBottomStyle.transform.matrix = mtx;
			activeBottomStyle.transform.matrix = mtx;
			bottomBtn = new SimpleButton(normalBottomStyle,normalBottomStyle,activeBottomStyle,normalBottomStyle);
			list.addChild(leftBtn);
			list.addChild(rightBtn);
			list.addChild(topBtn);
			list.addChild(bottomBtn);
			
			sceneImg = new Bitmap();
			sceneBox = new Sprite();
			sceneBox.addChild(sceneImg);
			scene.addChild(sceneBox);
			
			scene.addEventListener(MouseEvent.MOUSE_DOWN, beginChange);
			
			layout();
			
			timer = new Timer(10);
			
			var addType:int = 0;
			switch(align) {
				case TOP:
				case BOTTOM:
					addType = 0;
					leftBtn.addEventListener(MouseEvent.MOUSE_DOWN, scrollLeft);
					rightBtn.addEventListener(MouseEvent.MOUSE_DOWN, scrollRight);
					break;
				case LEFT:
				case RIGHT:
					addType = 1;
					topBtn.addEventListener(MouseEvent.MOUSE_DOWN, scrollTop);
					bottomBtn.addEventListener(MouseEvent.MOUSE_DOWN, scrollBottom);
					break;
			}
			for (var i:int = 0; i < photos.length; i++ ) {
				addToBox(new Image(photos[i],addType==0?-1:listBox.scrollRect.width,addType==1?-1:listBox.scrollRect.height, {index:i}));
			}
			
		}
		
		public function resetPhoto(pics:Array):void {
			var rect:Rectangle = sceneBox.scrollRect;
			var self:AlbumPane;
			this.photos = pics.filter(function(item:*, index:int, array:Array):Boolean {
				return String(item).replace(/\s*/g) != '';
			});
			var t:TweenNano=TweenNano.to(sceneImg, .3, {alpha:0, onComplete:function():void {
				t.kill();
				sceneImg.bitmapData.dispose();
				t = TweenNano.to(rect, .4, { x:0, y:0, width:0, height:0, onUpdate:function():void {
					sceneBox.scrollRect = rect;
					sceneBox.x = (sWidth - rect.width) * .5;
					sceneBox.y = (sHeight - rect.height) * .5;
				},onComplete:function():void {
					t.kill();
					listBoxWidth = 0;
					listBoxHeight = 0;
					var addType:int = 0;
					switch(align) {
						case TOP:
						case BOTTOM:
							addType = 0;
							break;
						case LEFT:
						case RIGHT:
							addType = 1;
							break;
					}
					for (var i:int = 0; i < photos.length; i++ ) {
						addToBox(new Image(photos[i],addType==0?-1:listBox.scrollRect.width,addType==1?-1:listBox.scrollRect.height, {index:i}));
					}
				}});
			}} );
			for (var i:int = 0; i < listBox.numChildren; i++ ) {
				(function(child:Image):void { 
					var t:TweenNano = TweenNano.to(child, .4, {
						x:0, y:0,
						alpha:0,
						onComplete:function():void {
							listBox.removeChild(child);
							t.kill();
						}
						} );
				} )(listBox.getChildAt(i));
			}
		}
		
		public function addToBox(img:Image):void {
			img.alpha = 0;
			img.addEventListener(ImageEvent.ONLOAD,function(e:ImageEvent):void{
				img.data.loaded = true;
				setImgPos();
				img.addEventListener(MouseEvent.MOUSE_DOWN, function(e:MouseEvent):void {
					e.stopPropagation();
					e.preventDefault();
				});
				img.addEventListener(MouseEvent.CLICK, showImage);
				if (img.data.index==0) {
					img.dispatchEvent(new MouseEvent(MouseEvent.CLICK));
				}
			});
			listBox.addChild(img);
		}
		
		public function setImgPos():void {
			var img:Image;
			var tx:Number, ty:Number;
			listBoxWidth = 0;
			listBoxHeight = 0;
			for (var i:int = 0; i < listBox.numChildren; i++ ) {
				img = listBox.getChildAt(i) as Image;
				if (img.data.loaded) {
					switch(align) {
						case TOP:
						case BOTTOM:
							listBoxWidth += space;
							ty = 0;
							tx = listBoxWidth;
							listBoxWidth += img.width;
							break;
						case LEFT:
						case RIGHT:
							listBoxHeight += space;
							ty = listBoxHeight;
							tx = 0;
							listBoxHeight += img.height;
							break;
					}
					if (tx != img.x || ty != img.y) {
						TweenNano.to(img, .5, { alpha:1, x:tx, y:ty } );
					}
				}
			}
		}
		
		public function showImage(e:MouseEvent):void {
			if (activeImg) {
				activeImg.deActive();
			}
			activeImg = e.target as Image;
			activeImg.active();
			fixScroll('auto',activeImg.x - (listBox.scrollRect.width - activeImg.width) * .5);
			var bmp:BitmapData = activeImg.bitmap.bitmapData;
			var bmpWidth:Number = bmp.width, bmpHeight:Number = bmp.height;
			var scale:Number = Math.min((sWidth-10) / bmpWidth, (sHeight-10) / bmpHeight);
			if (scale > 1) {
				scale = 1;
			}
			var newWidth:Number = bmpWidth * scale, newHeight:Number = bmpHeight * scale;
			var oldw:Number = sceneImg.width, oldh:Number = sceneImg.height;
			var t:TweenNano = TweenNano.to(sceneImg, .4, { alpha:0, onComplete:function():void {
				t.kill();
				var sr:Rectangle = sceneBox.scrollRect;
				sceneBox.graphics.clear();
				sceneBox.graphics.beginFill(0xffffff, .6);
				sceneBox.graphics.drawRect( -5, -5, Math.max(newWidth,oldw) + 10, Math.max(newHeight,oldh) + 10);
				sceneBox.graphics.endFill();
				t = TweenNano.to(sr, .3, { x:-5, y:-5, width:newWidth+10, height:newHeight+10,ease:Quart.easeOut, onUpdate:function():void {
					sceneBox.scrollRect = sr;
					sceneBox.x = (sWidth - sr.width) * .5;
					sceneBox.y = (sHeight - sr.height) * .5;
				}, onComplete:function():void {
					t.kill();
					sceneImg.bitmapData = bmp;
					sceneImg.scaleX = sceneImg.scaleY = scale;
					t = TweenNano.to(sceneImg, .4, { alpha:1, onComplete:function():void {
						t.kill();
					}});
				}} );
				checkLeftRight();
			}});
		}
		
		public function checkLeftRight(notCheck:String=""):void {
			if (activeImg.data.index > 0 && notCheck != "left") {
				var bmpd:BitmapData = (listBox.getChildAt(activeImg.data.index - 1) as Image).bitmap.bitmapData;
				if (leftBox) {
					if (!leftBox.stage) {
						scene.addChild(leftBox);
					}
					if(leftImg){
						leftImg.bitmapData = bmpd;
					}else {
						leftImg = new Bitmap(bmpd);
					}
				}else{
					leftBox = new Sprite();
					leftBox.addChild(leftImg = new Bitmap(bmpd));
					leftImg.smoothing = true;
					scene.addChild(leftBox);
				}
				var scaleNow:Number = Math.min((sWidth - 10) / bmpd.width, (sHeight - 10) / bmpd.height);
				if (scaleNow > 1) {
					scaleNow = 1;
				}
				leftImg.scaleX = leftImg.scaleY = scaleNow;
				leftBox.scrollRect = new Rectangle(-5,-5,leftImg.width + 10, leftImg.height + 10);
				leftBox.graphics.clear();
				leftBox.graphics.beginFill(0xffffff, .6);
				leftBox.graphics.drawRect( -5, -5, leftImg.width + 10, leftImg.height + 10);
				leftBox.graphics.endFill();
				leftBox.x = (sWidth - leftBox.width) * .5 - sWidth;
				leftBox.y = (sHeight - leftBox.height) * .5;
				
			}else if(notCheck != "left"){
				if (leftBox) {
					if(leftBox.stage){
						scene.removeChild(leftBox);
					}
					leftBox = null;
				}
			}
			if(activeImg.data.index < listBox.numChildren-1 && notCheck != "right"){
				bmpd= (listBox.getChildAt(activeImg.data.index + 1) as Image).bitmap.bitmapData;
				if (rightBox) {
					if (!rightBox.stage) {
						scene.addChild(rightBox);
					}
					if(rightImg){
						rightImg.bitmapData = bmpd;
					}else {
						rightImg = new Bitmap(bmpd);
					}
				}else{
					rightBox = new Sprite();
					rightBox.addChild(rightImg = new Bitmap(bmpd));
					rightImg.smoothing = true;
					scene.addChild(rightBox);
				}
				scaleNow = Math.min((sWidth - 10) / bmpd.width, (sHeight - 10) / bmpd.height);
				if (scaleNow > 1) {
					scaleNow = 1;
				}
				rightImg.scaleX = rightImg.scaleY = scaleNow;	
				rightBox.scrollRect = new Rectangle(-5,-5,rightBox.width + 10, rightBox.height + 10);
				rightBox.graphics.clear();
				rightBox.graphics.beginFill(0xffffff, .6);
				rightBox.graphics.drawRect( -5, -5, rightBox.width + 10, rightBox.height + 10);
				rightBox.graphics.endFill();
				rightBox.x = (sWidth - rightBox.width) * .5 + sWidth;
				rightBox.y = (sHeight - rightBox.height) * .5;
			}else if(notCheck != "right"){
				if (rightBox) {
					if(rightBox.stage){
						scene.removeChild(rightBox);
					}
					rightBox = null;
				}
			}
		}
		
		
		//滑动切换
		public function beginChange(e:MouseEvent):void {
			var startX:Number = e.stageX;
			var sPos:Number = sceneBox.x;
			var lPos:Number = 0;
			if (leftBox) {
				lPos=leftBox.x;
			}
			var rPos:Number = 0;
			if (rightBox) {
				rPos=rightBox.x;
			}
			var t:TweenNano;
			
			var moveHandle:Function = function(e:MouseEvent):void {
				var m:Number = e.stageX - startX;
				sceneBox.x = sPos + m;
				if (leftBox) {
					leftBox.x = lPos + m;
				}
				if (rightBox) {
					rightBox.x = rPos + m;
				}
			};
			var restore:Function = function():void {
				t=TweenNano.to(sceneBox, .5, { x:sPos, ease:Quart.easeOut, onUpdate:function():void {
					var m:Number = sceneBox.x - sPos;
					trace("update:",m);
					if (leftBox) {
						leftBox.x = lPos + m;
					}
					if (rightBox) {
						rightBox.x = rPos + m;
					}
				},onComplete:function():void {
					t.kill();
					scene.addEventListener(MouseEvent.MOUSE_DOWN, beginChange);
				}});
			}
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE, moveHandle);
			
			stage.addEventListener(MouseEvent.MOUSE_UP, function(e:MouseEvent):void {
				stage.removeEventListener(MouseEvent.MOUSE_MOVE, moveHandle);
				stage.removeEventListener(MouseEvent.MOUSE_UP, arguments.callee);
				scene.removeEventListener(MouseEvent.MOUSE_DOWN, beginChange);
				var m:Number = e.stageX - startX;
				trace(m);
				if (m < -10 && rightBox) {	//向左滑
					if (activeImg) {
						activeImg.deActive();
					}
					activeImg = listBox.getChildAt(activeImg.data.index + 1) as Image;
					activeImg.active();
					fixScroll('auto',activeImg.x - (listBox.scrollRect.width - activeImg.width) * .5);
					t=TweenNano.to(sceneBox, .5, { x:sPos-sWidth, onUpdate:function():void {
						var m:Number = sceneBox.x - sPos;
						if (leftBox) {
							leftBox.x = lPos + m;
						}
						if (rightBox) {
							rightBox.x = rPos + m;
						}
					},onComplete:function():void {
						t.kill();
							
						if (leftBox && leftBox.stage) {
							scene.removeChild(leftBox);
						}
						leftBox = sceneBox;
						leftImg = sceneImg;
						sceneBox = rightBox;
						sceneImg = rightImg;
						rightBox = null;
						
						checkLeftRight("left");
						scene.addEventListener(MouseEvent.MOUSE_DOWN, beginChange);
					}});
				}else if (m > 10 && leftBox) {	//向右滑
					if (activeImg) {
						activeImg.deActive();
					}
					activeImg = listBox.getChildAt(activeImg.data.index - 1) as Image;
					activeImg.active();
					fixScroll('auto',activeImg.x - (listBox.scrollRect.width - activeImg.width) * .5);
					t=TweenNano.to(sceneBox, .5, { x:sPos+sWidth, onUpdate:function():void {
						var m:Number = sceneBox.x - sPos;
						if (leftBox) {
							leftBox.x = lPos + m;
						}
						if (rightBox) {
							rightBox.x = rPos + m;
						}
					},onComplete:function():void {
						t.kill();
						if (rightBox && rightBox.stage) {
							scene.removeChild(rightBox);
						}
						rightBox = sceneBox;
						rightImg = sceneImg;
						sceneBox = leftBox;
						sceneImg = leftImg;
						leftBox = null;
						
						checkLeftRight("right");
						scene.addEventListener(MouseEvent.MOUSE_DOWN, beginChange);
					}});
				}else {
					restore();
				}
			});
		}
		
		public function fixScroll(type:String, tgt:Number=0):void {
			var rect:Rectangle = listBox.scrollRect;
			var cx:Number = rect.x, cy:Number = rect.y;
			var tx:Number = 0, ty:Number = 0, i:int;
			if (type == 'auto') {
				switch(align) {
					case AlbumPane.LEFT:
					case AlbumPane.RIGHT:
						cy = tgt;
						type = AlbumPane.TOP;
						break;
					case AlbumPane.TOP:
					case AlbumPane.BOTTOM:
						cx = tgt;
						type = AlbumPane.LEFT;
						break;
				}
			}
			switch(type) {
				case AlbumPane.LEFT:
					for (i = listBox.numChildren-1; i >=0; i-- ) {
						if (listBox.getChildAt(i).x < cx) {
							tx = listBox.getChildAt(i).x;
							break;
						}
					}
					
					break;
				case AlbumPane.RIGHT:
					for ( i = 0; i <listBox.numChildren; i++ ) {
						if (listBox.getChildAt(i).x > cx) {
							tx = listBox.getChildAt(i).x;
							break;
						}
					}
					break;
				case AlbumPane.TOP:
					for ( i = listBox.numChildren-1; i > 0; i-- ) {
						if (listBox.getChildAt(i).y < cy) {
							ty = listBox.getChildAt(i).y;
							break;
						}
					}
					
					break;
				case AlbumPane.BOTTOM:
					for (i = 0; i < listBox.numChildren; i++ ) {
						if (listBox.getChildAt(i).y > cy) {
							ty = listBox.getChildAt(i).y;
							break;
						}
					}
					break;
			}
			if (tx > listBoxWidth - listBox.scrollRect.width) {
				tx = listBoxWidth - listBox.scrollRect.width;
			}
			if (tx < 0) {
				tx = 0;
			}
			if (ty > listBoxHeight - listBox.scrollRect.height) {
				ty = listBoxHeight - listBox.scrollRect.height;
			}
			if (ty < 0) {
				ty = 0;
			}
			var t:TweenNano = TweenNano.to(rect, .3, { x:tx, y:ty, onUpdate:function():void {
				listBox.scrollRect = rect;
			},onComplete:function():void {
				t.kill();
			}});
		}
		
		public function uptimerHandler(f:Function = null):void {
			timer.reset();
			if (timerHandler!==null) {
				timer.removeEventListener(TimerEvent.TIMER, timerHandler);
			}
			timerHandler = f;
			if (timerHandler!==null) {
				timer.addEventListener(TimerEvent.TIMER, timerHandler);
				timer.start();
			}
		}
		
		public function scrollLeft(e:MouseEvent=null):void {
			uptimerHandler(function():void{
				var rect:Rectangle = listBox.scrollRect;
				rect.x -= 2;
				if (rect.x > 0) {
					listBox.scrollRect = rect;
				}
				
			});
			stage.addEventListener(MouseEvent.MOUSE_UP, function(e:MouseEvent):void {
				uptimerHandler();
				fixScroll(AlbumPane.LEFT);
				stage.removeEventListener(MouseEvent.MOUSE_UP, arguments.callee);
			});
		}
		public function scrollRight(e:MouseEvent = null):void {
			uptimerHandler(function():void {
				var rect:Rectangle = listBox.scrollRect;
				rect.x += 2;
				if (rect.x < listBoxWidth-rect.width) {
					listBox.scrollRect = rect;
				}
				
			});
			stage.addEventListener(MouseEvent.MOUSE_UP, function(e:MouseEvent):void {
				uptimerHandler();
				fixScroll(AlbumPane.RIGHT);
				stage.removeEventListener(MouseEvent.MOUSE_UP, arguments.callee);
			});
		}
		public function scrollTop(e:MouseEvent=null):void {
			uptimerHandler(function():void {
				var rect:Rectangle = listBox.scrollRect;
				rect.y -= 2;
				if (rect.y > 0) {
					listBox.scrollRect = rect;
				}
				
			});
			stage.addEventListener(MouseEvent.MOUSE_UP, function(e:MouseEvent):void {
				uptimerHandler();
				fixScroll(AlbumPane.TOP);
				stage.removeEventListener(MouseEvent.MOUSE_UP, arguments.callee);
			});
		}
		public function scrollBottom(e:MouseEvent=null):void {
			uptimerHandler(function():void {
				var rect:Rectangle = listBox.scrollRect;
				rect.y += 2;
				if (rect.y < listBoxHeight - rect.height) {
					listBox.scrollRect = rect;
				}
				
			});
			stage.addEventListener(MouseEvent.MOUSE_UP, function(e:MouseEvent):void {
				uptimerHandler();
				fixScroll(AlbumPane.BOTTOM);
				stage.removeEventListener(MouseEvent.MOUSE_UP, arguments.callee);
			});
		}
		
		public function layout():void {
			switch(align) {
				case TOP:
				case BOTTOM:
					leftBtn.visible = true;
					rightBtn.visible = true;
					topBtn.visible = false;
					bottomBtn.visible = false;
					list.scrollRect = new Rectangle(0, 0, aWidth, aHeight - sHeight - space);
					listBox.scrollRect = new Rectangle(0, 0, aWidth-leftBtn.width-rightBtn.width, aHeight - sHeight - space);
					break;
				case LEFT:
				case RIGHT:
					leftBtn.visible = false;
					rightBtn.visible = false;
					topBtn.visible = true;
					bottomBtn.visible = true;
					list.scrollRect = new Rectangle(0, 0, aWidth - sWidth - space, aHeight);
					listBox.scrollRect = new Rectangle(0, 0, aWidth - sWidth - space, aHeight-topBtn.height-bottomBtn.height);
					break;
			}
			switch(align) {
				case TOP:
					scene.x = 0;
					scene.y = aHeight - sHeight;
					list.x = 0;
					list.y = 0;
					listBox.x = leftBtn.width;
					listBox.y = 0;
					break;
				case BOTTOM:
					scene.x = 0;
					scene.y = 0;
					list.x = 0;
					listBox.x = leftBtn.width;
					listBox.y = 0;
					list.y = sHeight + space;
					break;
				case LEFT:
					scene.x = aWidth - sWidth;
					scene.y = 0;
					list.x = 0;
					list.y = 0;
					listBox.x = 0;
					listBox.y = topBtn.height;
					break;
				case RIGHT:
					scene.x = 0;
					scene.y = 0;
					list.x = sWidth + space;
					list.y = 0;
					listBox.x = 0;
					listBox.y = topBtn.height;
					break;
			}
			
			scene.scrollRect = new Rectangle(0, 0, sWidth, sHeight);

			leftBtn.x = 0;
			leftBtn.y = (list.scrollRect.height - leftBtn.height) * .5;
			rightBtn.x = list.scrollRect.width - leftBtn.width;
			rightBtn.y = (list.scrollRect.height - leftBtn.height) * .5;
			topBtn.x = (list.scrollRect.width - topBtn.width) * .5;
			topBtn.y = 0;
			bottomBtn.x = (list.scrollRect.width - bottomBtn.width) * .5;
			bottomBtn.y = list.scrollRect.height - bottomBtn.height;
			
			sceneBox.x = sWidth * .5;
			sceneBox.y = sHeight * .5;
			sceneBox.scrollRect = new Rectangle(0, 0, 0, 0);
		}
		
		public function terminate(e:Event):void {
			this.addEventListener(Event.ADDED_TO_STAGE, initialize);
			while (this.numChildren > 0) {
				removeChildAt(0);
			}
		}
		
	}

}