﻿package com.shirne.component {
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.display.Graphics;
	import flash.display.StageDisplayState;
	import com.greensock.TweenNano;
	import flash.filters.GlowFilter;
	
	public class FullScreen extends Sprite{
		
		public static const RIGHT_TOP:String = 'rt';
		public static const RIGHT_BOTTOM:String='rb';
		public static const LEFT_TOP:String='lt';
		public static const LEFT_BOTTOM:String = 'lb';
		
		public static const FULL_SCREEN_TYPE:String = StageDisplayState.FULL_SCREEN;
		
		private var color:uint;
		private var position:String;
		private var padding:int;
		
		private var size:int=20;
		
		private var glowFilter:GlowFilter=null;
		
		public function get isFullScreen():Boolean{
			return stage.displayState==StageDisplayState.FULL_SCREEN;
		}

		public function FullScreen(color:uint = 0x32ebfb, position:String='rt', padding:int = 10) {
			this.color = color;
			this.position = position;
			this.padding = padding;
			
			this.buttonMode = true;
			this.addEventListener(Event.ADDED_TO_STAGE,initialize);
		}
		
		public function initialize(e:Event):void{
			this.removeEventListener(Event.ADDED_TO_STAGE,initialize);
			this.addEventListener(Event.REMOVED_FROM_STAGE,terminate);
			
			doPosition();
			stage.addEventListener(Event.RESIZE,doPosition);
			
			glowFilter = new GlowFilter(this.color,.4,0,0,0,3);
			
			drawNormal(this.graphics);
			downLight();
			
			this.addEventListener(MouseEvent.MOUSE_OVER,highLight);
			this.addEventListener(MouseEvent.MOUSE_OUT,downLight);
			
			this.addEventListener(MouseEvent.CLICK,doFullScreen);
			
		}
		
		public function terminate(e:Event):void{
			this.removeEventListener(Event.REMOVED_FROM_STAGE,terminate);
			this.addEventListener(Event.ADDED_TO_STAGE,initialize);
		}
		
		public function doFullScreen(e:Event):void{
			if(stage.displayState != StageDisplayState.NORMAL){
				stage.displayState = StageDisplayState.NORMAL;
				drawNormal(this.graphics);
			}else{
				stage.displayState = FULL_SCREEN_TYPE;
				drawFull(this.graphics);
			}
		}
		
		public function downLight(e:Event=null):void{
			TweenNano.to(this,.4,{alpha:.5});
			TweenNano.to(this.glowFilter,1,{blurX:0,blurY:0,strength:0,onUpdate:applyFilter});
		}
		
		
		public function highLight(e:Event=null):void{
			TweenNano.to(this,.4,{alpha:1});
			TweenNano.to(this.glowFilter,1,{blurX:this.size * .3,blurY:this.size * .3,strength:this.size * .15,onUpdate:applyFilter});
		}
		
		private function applyFilter(e:Event=null):void{
			this.filters=[glowFilter];
		}
		
		public function drawNormal(g:Graphics):void{
			g.clear();
			g.lineStyle(1, this.color, 1,true);
			g.beginFill(0x000000,0);
			g.drawRect(1,1,this.size - 1, this.size - 1);
			g.endFill();
			
			g.moveTo(12, 3);
			g.lineTo(18,3);
			g.lineTo(18,9);
			
			g.moveTo(15,4);
			g.lineTo(17,4);
			g.lineTo(17,6);
			
			g.moveTo(16,5);
			g.lineTo(12,9);
			
			g.moveTo(3, 12);
			g.lineTo(3, 18);
			g.lineTo(9, 18);
			
			g.moveTo(4, 15);
			g.lineTo(4, 17);
			g.lineTo(6, 17);
			
			g.moveTo(5, 16);
			g.lineTo(9, 12);
			
		}
		
		public function drawFull(g:Graphics):void{
			g.clear();
			g.lineStyle(1, this.color, 1, true);
			g.beginFill(0x000000,0);
			g.drawRect(1,1,this.size - 1, this.size - 1);
			g.endFill();
			g.moveTo(12, 3);
			g.lineTo(12,9);
			g.lineTo(18,9);
			
			g.moveTo(13,6);
			g.lineTo(13,8);
			g.lineTo(15,8);
			
			g.moveTo(14,7);
			g.lineTo(18,3);
			
			g.moveTo(3, 12);
			g.lineTo(9, 12);
			g.lineTo(9, 18);
			
			g.moveTo(6, 13);
			g.lineTo(8, 13);
			g.lineTo(8, 15);
			
			g.moveTo(7, 14);
			g.lineTo(3, 18);
		}
		
		//该定位仅兼容舞台 TOP_LEFT定位
		public function doPosition(e:Event=null):void{
			switch(position){
				case FullScreen.LEFT_BOTTOM:
					this.x = this.padding;
					this.y = stage.stageHeight - this.padding - this.size;
					break;
				case FullScreen.LEFT_TOP:
					this.x = this.padding;
					this.y = this.padding;
					break;
				case FullScreen.RIGHT_BOTTOM:
					this.x = stage.stageWidth - this.padding - this.size;
					this.y = stage.stageHeight - this.padding - this.size;
					break;
				case FullScreen.RIGHT_TOP:
					this.x = stage.stageWidth - this.padding - this.size;
					this.y = this.padding;
					break;
			}
		}

	}
	
}
