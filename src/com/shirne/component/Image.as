package com.shirne.component 
{
	import com.shirne.events.ImageEvent;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.PixelSnapping;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	
	/**
	 * ...
	 * @author Shirne
	 */
	public class Image extends Sprite 
	{
		private var _src:String;
		private var _bmp:Bitmap;
		
		private var _width:Number;
		private var _height:Number;
		
		private var originalwidth:Number = -1;
		private var originalheight:Number = -1;
		
		public var rType:String;
		
		private var act:Shape;
		
		public var data:Object = new Object();
		
		public static const RESIZE_INNER:String = 'inner';
		public static const RESIZE_OUTER:String = 'outer';
		
		[Embed(source = "/../lib/nophoto.png")]
		public static var noPhotoClass:Class;
		public static var noPhoto:BitmapData;
		noPhoto = new noPhotoClass().bitmapData;
		
		public function get src():String {
			return _src;
		}
		
		public function set src(s:String):void {
			_src = s;
			_width = -1;
			_height = -1;
			loadImage(_src, onload);
		}
		
		public override function get width():Number {
			return _width*scaleX;
		}
		public override function get height():Number {
			return _height*scaleY;
		}
		
		
		public override function set width(w:Number):void {
			_width = w/scaleX;
			resizeTo(_width, _height);
		}
		
		public override function set height(h:Number):void {
			_height = h/scaleY;
			resizeTo(_width, _height);
		}
		
		public function set background(color:uint):void {
			this.graphics.clear();
			this.graphics.beginFill(color);
			this.graphics.drawRect(0, 0, _width, _height);
			this.graphics.endFill();
		}
		
		public function get originalWidth():Number {
			return originalwidth;
		}
		public function get originalHeight():Number {
			return originalheight;
		}
		
		public function resizeTo(w:Number = -1, h:Number = -1):void {
			if (w != -1) {
				this._width = w;
			}
			if (h != -1) {
				this._height = h;
			}
			if (!_bmp) {
				return;
			}
			var scale:Number = Math.min(_width / originalwidth, _height / originalheight);
			if (scale > 0) {
				_bmp.scaleX = _bmp.scaleY = scale;
			}
			if (_width < 0) {
				_width = _bmp.width;
			}
			if (_height < 0) {
				_height = _bmp.height;
			}
			_bmp.x = (_width - _bmp.width) * .5;
			_bmp.y = (_height - _bmp.height) * .5;
			deActive();
		}
		
		public function get bitmap():Bitmap {
			return _bmp;
		}
		
		[Event(name="onload", type="events.ImageEvent")]
		public function Image(src:String, w:Number =-1,h:Number=-1, dat:Object=null , resizeType:String=RESIZE_INNER) 
		{
			this._src = src;
			this._width = w;
			this._height = h;
			this.rType = resizeType;
			if (dat) {
				this.data = dat;
			}
			this.addEventListener(Event.ADDED_TO_STAGE, initialize);
			this.addEventListener(Event.REMOVED_FROM_STAGE, terminate);
		}
		
		public function initialize(e:Event):void {
			this.removeEventListener(Event.ADDED_TO_STAGE, initialize);
			
			act = new Shape();
			addChild(act);
			
			//var self:Image = this;
			loadImage(_src, onload);
		}
		
		private function onload(bmpd:BitmapData):void {
			if(bmpd){
				originalwidth = bmpd.width;
				originalheight = bmpd.height;
				if (_bmp) {
					_bmp.bitmapData.dispose();
				}
				
				_bmp = new Bitmap(bmpd, PixelSnapping.ALWAYS, true);
				
				resize();
				
				addChild(_bmp);
				setChildIndex(_bmp, 0);
				deActive();
				dispatchEvent(new ImageEvent(ImageEvent.ONLOAD));
			}else {
				dispatchEvent(new IOErrorEvent(IOErrorEvent.IO_ERROR));
			}
		}
		
		/**
		 * 
		 * @param	src			图片路径
		 * @param	callBack	回调函数,接收一个bitmapdata对象
		 */
		public static function loadImage(src:String, callBack:Function):void {
			//trace("IMG:",src);
			var loader:Loader = new Loader();
			if (!src) src = '';
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, function():void {
				var bmp:BitmapData = new BitmapData(loader.content.width, loader.content.height,true, 0x00000000);
				bmp.draw(loader.content);
				loader.unload();
				callBack(bmp);
			});
			loader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, function(e:IOErrorEvent):void {
				callBack(noPhoto.clone());
			});
			loader.load(new URLRequest(src));
		}
		
		//缩放图片
		public function resize():void {
			var scale:Number = -1;
			switch(rType) {
				case RESIZE_OUTER:
					if (_width > 0 && _height > 0) {
						scale = Math.max(_width / originalwidth, _height / originalheight);
					}else if (_width > 0) {
						scale = _width / originalwidth;
					}else if (_height > 0) {
						scale = _height / originalheight;
					}
					this.scrollRect = new Rectangle(0, 0, _width, _height);
					break;
				case RESIZE_INNER:
				default:
					
					if (_width > 0 && _height > 0) {
						scale = Math.min(_width / originalwidth, _height / originalheight);
					}else if (_width > 0) {
						scale = _width / originalwidth;
					}else if (_height > 0) {
						scale = _height / originalheight;
					}
			}
			if (scale > 0) {
				_bmp.scaleX = _bmp.scaleY = scale;
			}
			if (_width < 0) {
				_width = _bmp.width;
			}
			if (_height < 0) {
				_height = _bmp.height;
			}
			_bmp.smoothing = true;
			_bmp.x = (_width - _bmp.width) * .5;
			_bmp.y = (_height - _bmp.height) * .5;
		}
		
		public function active():void {
			if(this.numChildren>0 && act && act.stage){
				setChildIndex(act, this.numChildren - 1);
			}
			act.graphics.clear();
			act.graphics.lineStyle(2, 0xff6600);
			act.graphics.drawRect(0, 0, _width, _height);
		}
		
		public function deActive():void {
			if(this.numChildren>0 && act && act.stage){
				setChildIndex(act, this.numChildren - 1);
			}
			act.graphics.clear();
			//act.graphics.lineStyle(2, 0xffffff, .3);
			//act.graphics.drawRect(0, 0, _width, _height);
		}
		
		public function terminate(e:Event):void {
			this.addEventListener(Event.ADDED_TO_STAGE, initialize);
			if(_bmp && _bmp.bitmapData)_bmp.bitmapData.dispose();
			while (this.numChildren > 0) {
				removeChildAt(0);
			}
		}
	}

}