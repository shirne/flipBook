﻿package com.shirne.component {
	import flash.events.Event;
	
	public class FullScreenEvent extends Event {
		public static const ON_FULL_SCREEN:String="onfullscreen";
		public static const ON_NORMAL_SCREEN:String="onnormalscreen";
		
		public function FullScreenEvent():void{
			
		}

	}
	
}
