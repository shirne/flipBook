﻿package com.shirne.component
{
	import flash.display.MovieClip;
	import flash.display.Shape;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.ProgressEvent;
	
	public class Loading extends MovieClip
	{
		public var color:uint=0xdc208a;
		public var size:int=50;
		public var num:int = 8;
		public var fontColor:uint=0;
		private var dots:Vector.<Shape>;
		private var percent:TextField=null;
		private var objListen:EventDispatcher=null
		
		public function Loading(size:int=50,color:uint=0xdc208a,fontColor:uint=0xffffff, listenObject:EventDispatcher=null):void
		{
			this.color=color;
			this.fontColor=fontColor;
			this.size=size;
			this.x=size*.5;
			this.y=size*.5;
			if (listenObject) {
				this.objListen = listenObject;
			}
			this.addEventListener(Event.ADDED_TO_STAGE,init);
			this.addEventListener(Event.REMOVED_FROM_STAGE,term);
		}
		
		public function init(e:Event=null):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE,init);
			var deg:Number=Math.PI / num;
			var dSize=Math.sin(deg)*size;
			dots = new Vector.<Shape>();
			for(var i:int=0;i < num;i++){
				var dot:Shape=new Shape();
				dot.x = Math.sin(i*deg*2)*size*.5 ;
				dot.y = Math.cos(i*deg*2)*size*.5 ;
				dot.graphics.beginFill(color,1)
				dot.graphics.drawCircle(size*.02,size*.02,dSize*.5-size*.04);
				dot.graphics.endFill();
				dot.alpha=(i+1) / num;
				dots.push(dot);
				addChild(dot);
			}
			this.listen();
			addEventListener(Event.ENTER_FRAME,role);
		}
		
		public function term(e:Event=null):void
		{
			removeEventListener(Event.ENTER_FRAME,role);
			if(objListen && objListen.hasEventListener(ProgressEvent.PROGRESS)){
				objListen.removeEventListener(ProgressEvent.PROGRESS,onProgress);
			}
			while(numChildren>0){
				removeChildAt(0);
			}
			dots = null;
			percent = null;
			this.addEventListener(Event.ADDED_TO_STAGE,init);
		}
		
		public function listen(obj:EventDispatcher=null):void{
			if(obj){
				this.objListen=obj;
			}
			
			if(this.objListen){
				this.objListen.addEventListener(ProgressEvent.PROGRESS,onProgress);
			}
		}
		
		public function onProgress(e:ProgressEvent):void{
			this.updatePercent(e.bytesLoaded * 100/e.bytesTotal);
		}
		
		public function updatePercent(pct:int=0):void
		{
			if(!percent){
				percent=new TextField();
				percent.autoSize=TextFieldAutoSize.LEFT;
				
				percent.defaultTextFormat = new TextFormat('Snap ITC',12,fontColor);
				addChild(percent);
			}
			
			percent.text = int(pct % 101)+'%';
			
			percent.x=-percent.width*.5;
			percent.y=-percent.height*.5;
		}
		
		public function role(e:Event=null):void
		{
			for(var i:int=0;i<dots.length;i++){
				var al:Number=dots[i].alpha;
				al -=.03;
				if(al < 0)al=1;
				dots[i].alpha=al;
			}
		}
	}
}
