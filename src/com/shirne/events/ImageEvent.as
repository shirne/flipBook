package com.shirne.events 
{
	import flash.events.Event;
	
	/**
	 * 图像事件
	 * @author Shirne http://www.shirne.com/
	 */
	public class ImageEvent extends Event 
	{
		public static const ONLOAD:String = "onload";
		
		public function ImageEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false):void
		{
			super(type, bubbles, cancelable);
		}
		
	}

}
