﻿package com.shirne.extend
{
	import flash.display.DisplayObject;
	import flash.geom.Point;
	import flash.display.DisplayObjectContainer;
	import flash.geom.Matrix;

	public class ExPoint
	{

		public function ExPoint()
		{
		}


		public static function RegPoint(obj:DisplayObjectContainer , point:Point):void
		{
			var tmp_point:Point = obj.parent.globalToLocal(obj.localToGlobal(point));
			var len:int = obj.numChildren;
			var tmp_obj:DisplayObject;
			while ( len )
			{
				tmp_obj = obj.getChildAt(len);
				tmp_obj.x -=  point.x;
				tmp_obj.y -=  point.y;
				len--;
			}
			obj.x = tmp_point.x;
			obj.y = tmp_point.y;
		}
		
		//另一种注册点方法
		public static function RegPointMatrix(obj:DisplayObject):void
		{
			var hw:int = obj.width * .5, hh:int = obj.height * .5;
			var mt:Matrix = obj.transform.matrix;
			mt.tx = hw;
			mt.ty = hh;
			mt.translate(-hw, -hh);
			obj.transform.matrix = mt;
		}
		

	}

}