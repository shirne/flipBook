﻿package com.shirne.extend
{
	import flash.display.Graphics;

	public class ExGraphics
	{

		public static function drawSector(obj:Graphics,x:Number=0,y:Number=0,radius:Number=100,fromRadian:Number=0,radian:Number=0):void
		{
			obj.moveTo(x,y);
			if(Math.abs(radian) > Math.PI * 2){
				obj.drawCircle(x,y,radius);
			}else{
				var n:int = Math.ceil(radian * 4 / Math.PI);
				var angleAvg:Number = radian / n;
				var angleMid:Number, bx:Number, by:Number,cx:Number, cy:Number;
				obj.lineTo(x + radius * Math.cos(fromRadian),y + radius * Math.sin(fromRadian));
				for (var i=1; i<=n; i++)
				{
					fromRadian +=  angleAvg;
					angleMid = fromRadian - angleAvg * .5;
					bx=x + radius * Math.cos(angleMid) / Math.cos(angleAvg * .5);
					by=y + radius * Math.sin(angleMid) / Math.cos(angleAvg * .5);
					cx = x + radius * Math.cos(fromRadian);
					cy = y + radius * Math.sin(fromRadian);
					obj.curveTo(bx,by,cx,cy);
				}
				obj.lineTo(x,y);
			}
		}
		
	}
}
